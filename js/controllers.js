function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

angular.module('starter.controllers', [])

.controller('pin', function($scope, $state, $http, $ionicPopup, $ionicModal, $ionicLoading, $rootScope) {
    if (localStorage['server']) {
        $state.go('login');
    }
    $scope.data = { pin: '' };
    $scope.getInfo = function(data) {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });


        $http.get("https://admin.staffconnectapp.com/api/" + data.pin)
            .then(function(result) {
                $rootScope.URL = result.data;
                localStorage['URL'] = result.data;
                getConfig();
            }, function(error) {
                $ionicLoading.hide();
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
            })


        function getConfig() {
            $http.get($rootScope.URL + 'config/' + data.pin)
                .then(function(result) {
                    $ionicLoading.hide();
                    localStorage['server'] = JSON.stringify(result.data);
                    $rootScope.server = result.data;
                    $rootScope.refreshConfig();
                    $state.go('login');
                }, function(error) {
                    $ionicLoading.hide();
                    if(error.status != 401 && error.status != 404) {
                        $ionicPopup.alert({
                            title: 'Error',
                            template: error.data,
                            okType: 'button-dark'
                        });
                    }
                })
        }
    }
})


.controller('login', function(LoginService, $scope, $state, $http, $ionicPopup, $ionicModal, $timeout, $ionicLoading, $rootScope) {
    if (!localStorage['server'] || !localStorage['URL']) {
        $state.go('pin');
    }
    if (localStorage['token'] && localStorage['user']) {
        $state.go('dashboard.home');
    }
    $scope.data = {
        username: '',
        password: ''
    }
    $scope.clear = function() {
        delete localStorage['server'];
        delete localStorage['URL'];
        $state.go('pin');
        $rootScope.server = null;
    }
    $scope.login = function(user) {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Logging In..'
        });
        user.username = user.username.toLowerCase();
        $http.post($rootScope.URL + 'auth/login', $scope.data)
            .then(function(result) {
                $ionicLoading.hide();
                localStorage['user'] = JSON.stringify(result.data.user);
                $rootScope.currentUser = JSON.parse(localStorage['user']);
                if (result.data.user.role == 'admin') {
                    if(result.data.user.tempPass){
                      $scope.changePassword(user, result.data.token, result.data.user);
                    } else {
                      localStorage['token'] = result.data.token;
                      $state.go('dashboard.home');
                    }
                } else {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'You must be an Admin to access this dashboard.',
                        okType: 'button-dark'
                    });
                }
            }, function(error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Error',
                    template: error.data,
                    okType: 'button-dark'
                });
            })
    }

    $scope.changePassword = function (user, token, currentUser) {

  		$scope.data = {
  			username:user.username,
  			oldPassword:user.password,
  			password:'',
  		};

  		var myPopup = $ionicPopup.show({
  			template: '<input class="changepasswordinput" type="password" ng-model="data.password" placeholder="Password" style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;"><br><input class="changepasswordinput" type="password" ng-model="data.confirmPassword" placeholder="Retype password" style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;">',
  			title: 'Change password',
  			subTitle: 'Please type a new password below to proceed.',
  			scope: $scope,
  			buttons: [
  			  { text: 'Cancel' },
  			  {
  			    text: '<b>Continue</b>',
  			    type: 'button-positive',
  			    onTap: function(e) {
  					if (!$scope.data.password || $scope.data.password != $scope.data.confirmPassword) {
  						//don't allow the user to close unless he enters wifi password
  						$ionicPopup.alert({
  							title: '!',
  							template: '<center>Passwords must match</center>'
  						});
  						e.preventDefault();
  					} else {

  						$ionicLoading.show();
  						// return
  				        LoginService.resetPassword($scope.data)
  				            .then(function(success){
  				            	myPopup.close();
                        localStorage['token'] = token;
                        $state.go('dashboard.home');
  							  	$ionicLoading.hide();
  				            }, function( errorMessage ) {
  				            	$ionicLoading.hide();
  				                console.warn( errorMessage );
  						        $ionicPopup.alert({
  						          title: "Password error",
  						          template: '<center>'+errorMessage.data+'</center>'
  						        });
  				            });
  				            e.preventDefault();

  			      	}
  			    }
  			  }
  			]
  		});

  	}

    $scope.requestPassword = function() {
  	  $scope.userObject = {}

  	  // An elaborate, custom popup
  	  var showRequestPass = $ionicPopup.show({
  		template: '<div>'+
  					'<span class="center-text">Please enter an email address for your account.</span>'+
  					'<br /><br /><input type="email" placeholder="Account email address" ng-model="userObject.username" class="fp-input" lowercased style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;">'+
  				  '</div>',
  	    title: 'Need your password?',
  	    subTitle: '',
  	    scope: $scope,
  	    buttons: [
  	      { text: 'Cancel' },
  	      {
  	        text: '<b>Ok</b>',
  	        type: 'button-positive',
  	        onTap: function(e) {

  			   	if ( !$scope.userObject.username || !validateEmail($scope.userObject.username) ) {
  					$ionicLoading.show({ template: 'Please enter a valid email address' });
  					$timeout(function(){
  						$ionicLoading.hide();
  					},1000);
  		            e.preventDefault();
  	          	} else {
  				  	showRequestPass.close();
  				  	$ionicLoading.show({ template: 'Please wait...' });
  				  	var user = { username: $scope.userObject.username };
  			        LoginService.resetPassword(user)
  			            .then(function(success){

  						  	$ionicLoading.hide();
  							$ionicPopup.alert({
  								title: '',
  								template: '<center>Instructions to retrieve your password have been sent to your email address.</center>'
  							});
  			            }, function( errorMessage ) {
  			            	$ionicLoading.hide();
  			                console.warn( errorMessage );
  					        $ionicPopup.alert({
  					          title: "Request failed",
  					          template: '<center>There is no account associated with this email address</center>'
  					        });
  			            });

  	          }
  	        }
  	      }
  	    ]
  	  });

  	 };

})


.controller('dashboard', function($scope, $http, $state, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup, $q, $timeout) {

    // Force logout function when Unauthorised
    var unauthorisedAttemptPopup;

    $rootScope.$on("signout", function(evt,data){
        if(!$rootScope.unauthorisedAttempt && !unauthorisedAttemptPopup){
            $rootScope.unauthorisedAttempt = true;
            localStorage.clear();
            $state.go('pin');
            unauthorisedAttemptPopup = $ionicPopup.alert({
                title: 'Unauthorised',
                template: 'Your session has expired. Please login again.',
                okType: 'button-dark'
            });
            $rootScope.unauthorisedAttempt = false;
            $timeout(function(){
              window.location.reload();
            },1500);
        }
    });

    // User logout function
    $scope.logOut = function() {
        $ionicPopup.confirm({
            title: 'Logout',
            template: 'Are you sure you want to log out?'
        }).then(function(res) {
            if (res) {
                $http.post($rootScope.URL + 'auth/logout')
                delete localStorage['token']
                delete localStorage['user']
                $state.go('login');
            }
        });
    }

    // Setting current state in scope
    $scope.currentState = $state.current.name;
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        $scope.currentState = toState.name;
        if($rootScope.config) $rootScope.currentFeature = $rootScope.config.features[toParams.featureIndex];
    })

    $scope.loadUsers = function() {
        var deferred = $q.defer();
        $scope.users = [];
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });
        $http.get($rootScope.URL + 'admin/users')
            .then(function(result) {
                $ionicLoading.hide();
                $scope.users = result.data;
                deferred.resolve($scope.users);
            }, function(error) {
                $ionicLoading.hide();
                console.log(error);
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
            });
        return deferred.promise;

    }

    $scope.loadPermissions = function() {
        var deferred = $q.defer();
        $scope.locations = [];
        $scope.departments = [];
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });
        $http.get($rootScope.URL + 'auth/locations')
            .then(function(result) {
                $scope.locations = result.data;
                $http.get($rootScope.URL + 'auth/departments')
                    .then(function(result) {
                        $ionicLoading.hide();
                        $scope.departments = result.data;
                        deferred.resolve();
                    }, function(error) {
                        $ionicLoading.hide();
                        console.log(error);
                        // $ionicPopup.alert({
                        //     title: 'Error',
                        //     template: error.data,
                        //     okType: 'button-dark'
                        // });
                        deferred.reject();
                    });
            }, function(error) {
                $ionicLoading.hide();
                console.log(error);
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
                deferred.reject();
            });
        return deferred.promise;
    }

    $scope.loadCommunities = function(type) {
        var deferred = $q.defer();
        $scope.users = [];
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });
        $http.get($rootScope.URL + 'communities/' + type)
            .then(function(result) {
                $ionicLoading.hide();
                $scope.communities = result.data;
                deferred.resolve();
            }, function(error) {
                $ionicLoading.hide();
                console.log(error);
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
                deferred.reject();
            });
        return deferred.promise;

    }
    $scope.loadUsers();
    $scope.loadPermissions();

    $scope.generateAvatarColor = function(fullName) {

      if(fullName) {
        var numColors = 11;
        var hash = 0;
        if(fullName.length == 0) return hash;
        for(i = 0; i < fullName.length; i++){
            char = fullName.charCodeAt(i);
            hash = ((hash<<5)-hash)+char;
            hash = hash & hash; // Convert to 32bit integer
        }
        hash = Math.abs(hash) % numColors;

        return hash;
      }

    }

})

.controller('userManagement', function($scope, $http, $state, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup, $ionicScrollDelegate) {

    $scope.loadUsers().then(function() {
        $ionicScrollDelegate.resize();
    });
    $ionicModal.fromTemplateUrl('templates/userManagement/editUser.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: false
    }).then(function(modal) {
        $scope.editUserModal = modal;
    });
    $scope.openEditUserModal = function(user) {
        $scope.current = user;
        $scope.editUserModal.show();
    };
    $scope.closeEditUserModal = function() {
        $scope.editUserModal.hide();
    };
    $ionicModal.fromTemplateUrl('templates/userManagement/addUser.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: false
    }).then(function(modal) {
        $scope.addUserModal = modal;
    });
    $scope.openAddUserModal = function(index) {
        $scope.current = {
          role: 'user',
          disabled: 'false'
        };
        $scope.addUserModal.show();
    };
    $scope.closeAddUserModal = function() {
        $scope.addUserModal.hide();
    };
    $scope.addUser = function(user) {
        $http.post($rootScope.URL + 'admin/users', { _id: user._id, username: user.username, password: user.password, first_name: user.first_name, last_name: user.last_name, department: user.department._id, location: user.location._id, role: user.role, disabled: user.disabled, info:{"created_method":"ac_single"} })
            .then(function(result) {
                $ionicLoading.hide();
                $scope.closeAddUserModal();
                $scope.loadUsers();
            }, function(error) {
                $ionicLoading.hide();
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
            });
    }
    $scope.saveUser = function(user) {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Updating...'
        });
        $http.put($rootScope.URL + 'admin/users', { _id: user._id, username: user.username, first_name: user.first_name, last_name: user.last_name, department: user.department._id, location: user.location._id, role: user.role, disabled: user.disabled})
            .then(function(result) {
                $ionicLoading.hide();
                $scope.closeEditUserModal();
                $scope.loadUsers();
            }, function(error) {
                $ionicLoading.hide();
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
            });
    }

    $scope.resendVerificationEmail = function(email) {
        $ionicPopup.confirm({
            title: 'Resend verification email',
            template: 'Are you sure you wish to resend the verification email for this user?'
        }).then(function(res) {
            if (res) {
                $ionicLoading.show({
                    template: '<ion-spinner icon="bubbles"></ion-spinner><br />Resending verification email..'
                });
                $http.post($rootScope.URL + 'auth/resendverify', { username: email }).then(function() {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Success',
                        template: 'Email verification sent.',
                        okType: 'button-dark'
                    });
                }, function(err) {
                    $ionicLoading.hide();
                    if(error.status != 401 && error.status != 404) {
                        $ionicPopup.alert({
                            title: 'Error',
                            template: error.data,
                            okType: 'button-dark'
                        });
                    }
                })
            }
        });
    }

    $scope.manuallyVerified = function(user) {
      $ionicPopup.confirm({
          title: 'Manually Verify',
          template: 'Are you sure you wish to manually verify this user?'
      }).then(function(res) {
          if (res) {
              $ionicLoading.show({
                  template: '<ion-spinner icon="bubbles"></ion-spinner><br />Updating...'
              });

              user.pin = null;
              $http.put($rootScope.URL + 'admin/users', { _id: user._id, username: user.username, first_name: user.first_name, last_name: user.last_name, department: user.department._id, location: user.location._id, role: user.role, disabled: user.disabled, pin: user.pin})
                  .then(function(result) {
                      $scope.current.pin = null;
                      $ionicLoading.hide();
                      $ionicPopup.alert({
                          title: 'Success',
                          template: 'Manually Verified.',
                          okType: 'button-dark'
                      });
                  }, function(error) {
                      $ionicLoading.hide();
                      if(error.status != 401) {
                          $ionicPopup.alert({
                              title: 'Error',
                              template: error.data,
                              okType: 'button-dark'
                          });
                      }
              });
          }
      });
    }

    $scope.manuallySetUserPassword = function(user) {

      $scope.data = {
  			username:user.username,
  			oldPassword:user.password,
  			password:'',
  		};

  		var myPopup = $ionicPopup.show({
  			template: '<input class="changepasswordinput" type="password" ng-model="data.password" placeholder="Password" style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;"><br><input class="changepasswordinput" type="password" ng-model="data.confirmPassword" placeholder="Retype password" style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;">',
  			title: 'Manually set user password',
  			subTitle: 'Please type a new password below to proceed.',
  			scope: $scope,
  			buttons: [
  			  { text: 'Cancel' },
  			  {
  			    text: '<b>Continue</b>',
  			    type: 'button-positive',
  			    onTap: function(e) {
  					if (!$scope.data.password || $scope.data.password != $scope.data.confirmPassword) {
  						//don't allow the user to close unless he enters wifi password
  						$ionicPopup.alert({
  							title: '!',
  							template: '<center>Passwords must match</center>'
  						});
  						e.preventDefault();
  					} else {

  						$ionicLoading.show();

              $http.put($rootScope.URL + 'admin/users', { _id: user._id, username: user.username, first_name: user.first_name, last_name: user.last_name, department: user.department._id, location: user.location._id, role: user.role, disabled: user.disabled, newPassword: $scope.data.password})
                  .then(function(result) {
                      $scope.current.pin = null;
                      $ionicLoading.hide();
                      $ionicPopup.alert({
                          title: 'Success',
                          template: 'Manually Set Password.',
                          okType: 'button-dark'
                      });
                  }, function(error) {
                      $ionicLoading.hide();
                      if(error.status != 401) {
                          $ionicPopup.alert({
                              title: 'Error',
                              template: error.data,
                              okType: 'button-dark'
                          });
                      }
              });

  			     }
  			    }
  			  }
  			]
  		});

    }

    $scope.resendAllVerificationEmails = function(email) {
        $ionicPopup.confirm({
            title: 'Resend verification email to all?',
            template: 'Are you sure you wish to resend the verification email for all users that have not verified yet?'
        }).then(function(res) {
            if (res) {
                $ionicLoading.show({
                    template: '<ion-spinner icon="bubbles"></ion-spinner><br />Resending verification email to all users who have not verified yet..'
                });
                $http.post($rootScope.URL + 'admin/remindusers').then(function(success) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: success.data,
                        template: 'Email verification sent to all users who have not verified yet.',
                        okType: 'button-dark'
                    });
                    $scope.disableResendAllVerificationEmails = success.data;
                }, function(err) {
                    $ionicLoading.hide();
                    if(error.status != 401 && error.status != 404) {
                        $ionicPopup.alert({
                            title: 'Error',
                            template: error.data,
                            okType: 'button-dark'
                        });
                    }
                })
            }
        });
    }

    // SECRET ADVANCED TAB ENTRY POINT
    var advancedCount = 0;
    $scope.goToAdvanced = function () {
        advancedCount++;
        if(advancedCount > 4) {
            $state.go("dashboard.userManagement-advanced")
        }
    }

})

.controller('bulkAddUser', function($scope, $http, $state, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup, $ionicScrollDelegate, Upload) {
    $scope.showUpload = false;
    $scope.loadBulkUser = function() {
        console.log('Loading USers');
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });
        $http.get($rootScope.URL + 'bulkUserUpload')
            .then(function(result) {
                $ionicLoading.hide();
                var users = result.data;
                var validUsers = [];
                var invalidUsers = [];
                for (i in users) {
                    if (users[i].location == false || users[i].department == false) {
                        invalidUsers.push(users[i]);
                    } else {
                        validUsers.push(users[i]);
                    }
                }
                $scope.validUsers = validUsers;
                $scope.invalidUsers = invalidUsers;
                $scope.showUpload = false;
                if (result.data.length == 0) $scope.showUpload = true;
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.loadBulkUser();
    $scope.save = function(user) {
        if (user.location == false) {
            if (!user.newLocation) return false;
            user.location = user.newLocation;
        }
        if (user.department == false) {
            if (!user.newDepartment) return false;
            user.department = user.newDepartment;
        }
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Saving...'
        });
        $http.put($rootScope.URL + 'bulkUserUpload', user)
            .then(function(result) {
                $ionicLoading.hide();
                $scope.loadBulkUser();
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.add = function() {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Adding...'
        });
        $http.post($rootScope.URL + 'bulkUserUpload')
            .then(function(result) {
                $ionicLoading.hide();
                $scope.loadBulkUser();
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.clear = function() {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Clearing...'
        });
        $http.delete($rootScope.URL + 'bulkUserUpload')
            .then(function(result) {
                $ionicLoading.hide();
                $scope.loadBulkUser();
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.upload = function(files) {
        Upload.upload({
            url: $rootScope.URL + 'bulkUserUpload/csv',
            data: { file: files }
        }).then(function(result) {
            $scope.loadBulkUser();
        }, function(resp) {
            $ionicLoading.hide();
        }, function(evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $ionicLoading.show({
                template: '<i class="icon ion-upload"></i><br />' + progressPercentage + '%'
            });
        });
    }
})

.controller('auth2', function($scope, $http, $state, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup, $ionicScrollDelegate, Upload) {
    $scope.showUpload = false;
    $scope.loadBulkUser = function() {
        console.log('Loading USers');
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });
        $http.get($rootScope.URL + 'auth2')
            .then(function(result) {
                $scope.availableUsers = result.data;
                $http.get($rootScope.URL + 'auth2/import')
                    .then(function(result) {
                        $ionicLoading.hide();
                        var users = result.data;
                        var validUsers = [];
                        var invalidUsers = [];
                        for (i in users) {
                            if (users[i].location == false || users[i].department == false || !users[i].data1 || !users[i].data2) {
                                invalidUsers.push(users[i]);
                            } else {
                                validUsers.push(users[i]);
                            }
                        }
                        $scope.validUsers = validUsers;
                        $scope.invalidUsers = invalidUsers;
                        $scope.showUpload = false;
                        if (result.data.length == 0) $scope.showUpload = true;
                    }, function(err) {
                        $ionicLoading.hide();
                        alert(err);
                    });
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.loadBulkUser();
    $scope.save = function(user) {
        if (user.location == false) {
            if (!user.newLocation) return false;
            user.location = user.newLocation;
        }
        if (user.department == false) {
            if (!user.newDepartment) return false;
            user.department = user.newDepartment;
        }
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Saving...'
        });
        $http.put($rootScope.URL + 'auth2/import', user)
            .then(function(result) {
                $ionicLoading.hide();
                $scope.loadBulkUser();
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.add = function() {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Adding...'
        });
        $http.post($rootScope.URL + 'auth2/import')
            .then(function(result) {
                $ionicLoading.hide();
                $scope.loadBulkUser();
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.clear = function() {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Clearing...'
        });
        $http.delete($rootScope.URL + 'auth2/import')
            .then(function(result) {
                $ionicLoading.hide();
                $scope.loadBulkUser();
            }, function(err) {
                $ionicLoading.hide();
                alert(err);
            });
    }
    $scope.upload = function(files) {
        Upload.upload({
            url: $rootScope.URL + 'auth2/import/csv',
            data: { file: files }
        }).then(function(result) {
            $scope.loadBulkUser();
        }, function(resp) {
            $ionicLoading.hide();
        }, function(evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $ionicLoading.show({
                template: '<i class="icon ion-upload"></i><br />' + progressPercentage + '%'
            });
        });
    }
})

.controller('permissions', function($scope, $http, $state, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup) {

    $scope.loadPermissions();

    $scope.addLocation = function() {
        $scope.data = { location: '' };
        $ionicPopup.show({
            template: '<input type="text" ng-model="data.location">',
            title: 'Add new location',
            subTitle: 'Please enter the name of the location',
            scope: $scope,
            buttons: [{
                text: 'Cancel',
                type: 'button-dark',
            }, {
                text: '<b>Add</b>',
                type: 'button-assertive',
                onTap: function(e) {
                    if (!$scope.data.location) {
                        e.preventDefault();
                    } else {
                        $ionicLoading.show({
                            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Adding...'
                        });
                        $http.post($rootScope.URL + 'auth/locations', { name: $scope.data.location })
                            .then(function(result) {
                                $ionicLoading.hide();
                                $scope.loadPermissions();
                            }, function(error) {
                                $ionicLoading.hide();
                                if(error.status != 401 && error.status != 404) {
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: error.data,
                                        okType: 'button-dark'
                                    });
                                }
                            });
                    }
                }
            }]
        });
    }
    $scope.editLocation = function(id) {

        for (var i = 0; i < $scope.locations.length; i++) {
            if ($scope.locations[i]._id == id) {
                $scope.data = $scope.locations[i];
            }
        }

        $ionicPopup.show({
            template: '<input type="text" ng-model="data.name">',
            title: 'Edit location',
            cssClass: 'widePopup',
            scope: $scope,
            buttons: [{
                text: 'Cancel',
                type: 'button-dark',
            }, {
                text: '<b>Delete</b>',
                type: 'button-delete',
                onTap: function(e) {
                    if (!$scope.data.name) {
                        e.preventDefault();
                    } else {
                        $ionicLoading.show({
                            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Deleting...'
                        });
                        $http.delete($rootScope.URL + 'auth/locations/' + $scope.data._id)
                            .then(function(result) {
                                $ionicLoading.hide();
                                $scope.loadPermissions();
                            }, function(error) {
                                $ionicLoading.hide();
                                if(error.status != 401 && error.status != 404) {
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: error.data,
                                        okType: 'button-dark'
                                    });
                                }
                            });
                    }
                }
            }, {
                text: '<b>Save</b>',
                type: 'button-assertive',
                onTap: function(e) {
                    if (!$scope.data.name) {
                        e.preventDefault();
                    } else {
                        $ionicLoading.show({
                            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Updating...'
                        });
                        $http.put($rootScope.URL + 'auth/locations', { _id: $scope.data._id, name: $scope.data.name })
                            .then(function(result) {
                                $ionicLoading.hide();
                                $scope.loadPermissions();
                            }, function(error) {
                                $ionicLoading.hide();
                                if(error.status != 401 && error.status != 404) {
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: error.data,
                                        okType: 'button-dark'
                                    });
                                }
                            });
                    }
                }
            }, ]
        });
    }

    $scope.addDepartment = function() {
        $scope.data = { location: '' };
        $ionicPopup.show({
            template: '<input type="text" ng-model="data.location">',
            title: 'Add new department',
            subTitle: 'Please enter the name of the department',
            scope: $scope,
            buttons: [{
                text: 'Cancel',
                type: 'button-dark',
            }, {
                text: '<b>Add</b>',
                type: 'button-assertive',
                onTap: function(e) {
                    if (!$scope.data.location) {
                        e.preventDefault();
                    } else {
                        $ionicLoading.show({
                            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Adding...'
                        });
                        $http.post($rootScope.URL + 'auth/departments', { name: $scope.data.location })
                            .then(function(result) {
                                $ionicLoading.hide();
                                $scope.loadPermissions();
                            }, function(error) {
                                $ionicLoading.hide();
                                if(error.status != 401 && error.status != 404) {
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: error.data,
                                        okType: 'button-dark'
                                    });
                                }
                            });
                    }
                }
            }]
        });
    }
    $scope.editDepartment = function(id) {

        for (var i = 0; i < $scope.departments.length; i++) {
            if ($scope.departments[i]._id == id) {
                $scope.data = $scope.departments[i];
            }
        }

        $ionicPopup.show({
            template: '<input type="text" ng-model="data.name">',
            title: 'Edit department',
            cssClass: 'widePopup',
            scope: $scope,
            buttons: [{
                text: 'Cancel',
                type: 'button-dark',
            }, {
                text: '<b>Delete</b>',
                type: 'button-delete',
                onTap: function(e) {
                    if (!$scope.data.name) {
                        e.preventDefault();
                    } else {
                        $ionicLoading.show({
                            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Deleting...'
                        });
                        $http.delete($rootScope.URL + 'auth/departments/' + $scope.data._id)
                            .then(function(result) {
                                $ionicLoading.hide();
                                $scope.loadPermissions();
                            }, function(error) {
                                $ionicLoading.hide();
                                if(error.status != 401 && error.status != 404) {
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: error.data,
                                        okType: 'button-dark'
                                    });
                                }
                            });
                    }
                }
            }, {
                text: '<b>Save</b>',
                type: 'button-assertive',
                onTap: function(e) {
                    if (!$scope.data.name) {
                        e.preventDefault();
                    } else {
                        $ionicLoading.show({
                            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Updating...'
                        });
                        $http.put($rootScope.URL + 'auth/departments', { _id: $scope.data._id, name: $scope.data.name })
                            .then(function(result) {
                                $ionicLoading.hide();
                                $scope.loadPermissions();
                            }, function(error) {
                                $ionicLoading.hide();
                                if(error.status != 401 && error.status != 404) {
                                    $ionicPopup.alert({
                                        title: 'Error',
                                        template: error.data,
                                        okType: 'button-dark'
                                    });
                                }
                            });
                    }
                }
            }]
        });
    }
})

.controller('clients', function($scope, $http, $state, $stateParams, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup, $q) {
    $scope.loadClients = function() {
        var deferred = $q.defer();
        $scope.clients = [];
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });
        $http.get($rootScope.URL + 'admin/clients')
            .then(function(result) {
                $ionicLoading.hide();
                $scope.clients = result.data;
                deferred.resolve();
            }, function(error) {
                $ionicLoading.hide();
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
                deferred.reject();
            });
        return deferred.promise;

    }
    $scope.loadClients();

    $ionicModal.fromTemplateUrl('templates/clients/add.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.addModal = modal;
    });

    $scope.addClient = function() {
        $scope.client = {};
        $scope.addModal.show();
    };
    $scope.hideAddClient = function() {
        $scope.addModal.hide();
    };

    $scope.add = function(client, cloneFrom) {
      // $ionicLoading.show({
      //     template: '<ion-spinner icon="bubbles"></ion-spinner><br />Saving...'
      // });
      if(cloneFrom) {
        for (var i = 0; i < $scope.clients.length; i++) {
          if($scope.clients[i]._id == cloneFrom){
              client.legacy = $scope.clients[i].legacy;
          }
        }
      } else {
        client.legacy = JSON.stringify(sampleConfig);
      }

      $http.post($rootScope.URL + 'admin/client', client)
          .then(function(result) {
              $scope.hideAddClient();
              $state.go('dashboard.client', { id: result.data })
              $ionicLoading.hide();
          }, function(error) {
              $ionicLoading.hide();
              if(error.status != 401 && error.status != 404) {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: error.data,
                      okType: 'button-dark'
                  });
              }
          });
    }

})

.controller('client', function(AnalyticsService, $scope, $http, $state, $stateParams, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup, $q, $ionicScrollDelegate, Upload) {

    // Load Google Analytics Properties
    (function(w, d, s, g, js, fjs) {
        g = w.gapi || (w.gapi = {});
        g.analytics = {
            q: [],
            ready: function(cb) {
                this.q.push(cb)
            }
        };
        js = d.createElement(s);
        fjs = d.getElementsByTagName(s)[0];
        js.src = 'https://apis.google.com/js/platform.js';
        fjs.parentNode.insertBefore(js, fjs);
        js.onload = function() {
            g.load('analytics')
        };
    }(window, document, 'script'));


    AnalyticsService.getGAKey()
        .then(function(success) {
            init(success.data);
        }, function(error) {
            console.log(error)
        })

    function init(gaKey) {
        gapi.analytics.auth.authorize({ serverAuth: { access_token: gaKey } });
        queryProperties();
    }

    function queryProperties(accountId) {
        // Get a list of all the properties for the account.
        gapi.client.analytics.management.webproperties.list({ 'accountId': '~all' })
            .then(function(success) {
                $scope.gaProperties = success.result.items;
            })
            .then(null, function(err) {
                console.log(err);
            });
    }




    function ConvertToCSV(obj) {
        var result = '';
        for (i in obj) {
            result = result + obj[i].title;
            if (i != obj.length - 1) {
                result = result + ',';
            }
        }
        return result;
    }

    function ConvertToObject(obj) {
        var result = [];
        var obj = obj.split(',');
        for (i in obj) {
            result.push({ "title": obj[i] });
        }
        return result;
    }
    $scope.loadClient = function(id) {
        var deferred = $q.defer();
        $scope.client = null;
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
        });
        $http.get($rootScope.URL + 'admin/client/' + id)
            .then(function(result) {
                $ionicLoading.hide();
                $scope.client = result.data;
                $scope.client.streams = ConvertToCSV($scope.client.streams);
                $scope.client.config = $scope.client.legacy ? JSON.parse($scope.client.legacy) : {};
                $scope.client.config.homescreen.panels = $scope.client.config.homescreen.panels ? $scope.client.config.homescreen.panels : [];
                deferred.resolve();
            }, function(error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Error',
                    template: error.data,
                    okType: 'button-dark'
                });
                deferred.reject();
            });
        return deferred.promise;

    }
    $scope.loadClient($stateParams.id);

    $scope.$watch('client', function(newVal, oldVal) {
        $ionicScrollDelegate.resize();
    }, true);

    $scope.save = function(client) {

        $scope.client.streams = ConvertToObject($scope.client.streams);
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Saving...'
        });

        if (client.config.signup && client.config.signup.methods.companydomain.acceptedDomains) {
            client.config.signup.methods.companydomain.acceptedDomains = typeof client.config.signup.methods.companydomain.acceptedDomains != 'string' ? client.config.signup.methods.companydomain.acceptedDomains : client.config.signup.methods.companydomain.acceptedDomains.split(",");
        }

        if(client.config.global){
         client.config.global.theme.popup = {title:'black'};
        }

        client.legacy = JSON.stringify(client.config);
        $http.put($rootScope.URL + 'admin/client', client)
            .then(function(result) {
                $ionicPopup.alert({
                    title: 'Saved.',
                    template: 'Saved Successfully.',
                    okType: 'button-dark'
                });
                $scope.client.streams = ConvertToCSV($scope.client.streams);
                $scope.loadClient($stateParams.id);
                $rootScope.refreshConfig();
                $ionicLoading.hide();
            }, function(error) {
                $ionicLoading.hide();
                $scope.client.streams = ConvertToCSV($scope.client.streams);
                if(error.status != 401 && error.status != 404) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
            });
    }

    $scope.uploadFiles = function(files) {
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />0%'
        });
        Upload.upload({
            url: $rootScope.URL + 'admin/client/upload',
            data: { file: files }
        }).then(function(result) {
            $ionicLoading.hide();
            $scope.client.icon = result.data;
            $scope.save($scope.client);
        }, function(resp) {
            $ionicLoading.hide();
            console.log(resp);
        }, function(evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $ionicLoading.show({
                template: '<i class="icon ion-upload"></i><br />' + progressPercentage + '%'
            });
        });
    };



    // Reorder features
    $scope.moveItem = function(item, fromIndex, toIndex) {

        //Move the item in the array
        $scope.client.config.features.splice(fromIndex, 1);
        $scope.client.config.features.splice(toIndex, 0, item);

        count = 0;
        for (var i = 0; i < $scope.client.config.features.length; i++) {
            $scope.client.config.features[i].index = count;
            count++;
        }

        console.log($scope.client.config.features)
    };



    // Add feature
    $ionicModal.fromTemplateUrl('templates/clients/addFeature.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: false
    }).then(function(modal) {
        $scope.addFeatureModal = modal;
    });
    $scope.openAddFeatureModal = function(index) {
        $scope.featureForm = {};
        $scope.addFeatureModal.show();
    };
    $scope.closeAddFeatureModal = function() {
        $scope.addFeatureModal.hide();
    };

    $scope.addFeature = function(feature) {
        if (!$scope.client.config.features) {
            $scope.client.config.features = [];
        }
        $scope.client.config.features.push(feature);
        count = 0;
        for (var i = 0; i < $scope.client.config.features.length; i++) {
            $scope.client.config.features[i].index = count;
            count++;
        }
        $scope.closeAddFeatureModal();
        console.log($scope.client.config.features)
    }


    // Edit feature
    $ionicModal.fromTemplateUrl('templates/clients/editFeature.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: false
    }).then(function(modal) {
        $scope.editFeatureModal = modal;
    });
    $scope.openEditFeatureModal = function(index) {
        $scope.featureForm = index;
        $scope.editFeatureModal.show();
    };
    $scope.closeEditFeatureModal = function() {
        $scope.editFeatureModal.hide();
    };

    // reset feature form
    $scope.resetFeatureForm = function(module) {
        $scope.featureForm = { "module": module };
        console.log($scope.featureForm)
    }

    // delete deature
    $scope.deleteFeature = function(index) {

        for (var i = 0; i < $scope.client.config.features.length; i++) {
            if ($scope.client.config.features[i].index == $scope.featureForm.index) {
                $scope.client.config.features.splice(i,1);
            }
        }
        $scope.closeEditFeatureModal();
        console.log($scope.client.config.features)
    }




})
