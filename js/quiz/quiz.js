

angular.module('starter.controllers')

    .controller('quiz', function($scope, $state, $stateParams, $http, $ionicPopup, $ionicModal, $ionicLoading, $rootScope) {

      $scope.currentState = $state.current.name;
      $scope.token = localStorage['token'];
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
        $scope.currentState = toState.name;
        $scope.token = localStorage['token'];
      })
      $scope.list = [];
        $http({
            method:"GET",
            headers:{
                "code":"scteam",
                "token":$scope.token
            },
            url: $rootScope.URL+'quiz/all',
            params: {},
            data: {}
        }).then(function(result) {
                $scope.list = result.data;
                console.log($scope.list);
                var count = 0;
                var length = result.data.length-1;
                getAnalytics(result.data[0].id);
                function getAnalytics(quizId){
                    $http({
                        method:"GET",
                        headers:{
                            "code":"scteam",
                            "token":$scope.token
                        },
                        url: $rootScope.URL+'quiz/analytics/'+quizId,
                        params: {},
                        data: {}
                    }).then(function(analyticsResult) {

                        for (var i = result.data.length - 1; i >= 0; i--) {
                            if(result.data[i].id == quizId){
                                result.data[i].timesTaken = analyticsResult.data.length;
                            }
                        };
                        if(count == length) {
                            $scope.list = result.data;
                        } else {
                            count++;
                            getAnalytics(result.data[count].id)
                        };

                    }, function(error) {
                        console.log(error);
                    });
                }

                $scope.list = result.data;
            }, function(error) {
                console.log(error);
            });

        //delete quiz

        $scope.remove = function(id) {
            for(var i in $scope.list){
                if($scope.list[i].id == id){
                    $http({
                        method:"DELETE",
                        headers:{
                            "code":"scteam",
                            "token":$scope.token,
                            "content-type":"application/json"
                        },
                        url: $rootScope.URL+ 'quiz/'+id,
                        params: {},
                        data: {id: id}
                    }).then(function(res) {
                        $state.go($state.current, {}, { reload: true });
                        console.log("success");
                    }, function(error) {
                        console.log(error);
                    });
                }
            }

        }
        //add new quiz
        $scope.quizAddNew = function(form){
             console.log(form);
                $scope.form = {
                    name: form.name,
                    type:0
                }
                $http({
                        method:"POST",
                        headers:{
                            "code":"scteam",
                            "token":$scope.token,
                            "content-type":"application/json"
                        },
                        url: $rootScope.URL+'quiz',
                        params: {},
                        data: $scope.form
                    }).then(function(result) {
                        $rootScope.insertedQuizId = result.data.insertedIds[0];
                        $state.go('dashboard.quiz-detail', {featureId:$stateParams.featureId, id:$rootScope.insertedQuizId});
                }, function(error) {
                    console.log(error);
            });
        }
        //Leaderboard
        $scope.currentState = $state.current.name;
        $scope.token = localStorage['token'];
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
            $scope.currentState = toState.name;
            $scope.token = localStorage['token'];
        })
         $scope.leaderboard = function(quizId){
            $ionicModal.fromTemplateUrl('templates/quiz/leaderboard.html', {
              scope: $scope,
              animation: 'slide-in-up'
            }).then(function(modal) {
              $scope.leaderboardModal = modal;
              $scope.leaderboardList = [];
                $http({
                    method:"GET",
                    headers:{
                        "code":"scteam",
                        "token":$scope.token
                    },
                    url: $rootScope.URL+'quiz/analytics/'+quizId,
                    params: {},
                    data: {}
                    }).then(function(result) {
                    $scope.leaderboardList = result.data;
                        $http({
                        method:"GET",
                        headers:{
                            "code":"scteam",
                            "token":$scope.token
                        },
                        url: $rootScope.URL+'quiz/'+quizId,
                        params: {},
                        data: {}
                        }).then(function(result2) {
                            $scope.leaderboardList2 = result2.data;
                            $scope.leaderboardTitle = $scope.leaderboardList2['info'][0]['name'];
                            $scope.quesLength = $scope.leaderboardList2['questions'].length;

                        }, function(error) {
                            console.log(error);
                        });

                }, function(error) {
                    console.log(error);
                });
                $scope.leaderboardModal.show();

            });
        }
        //end of leaderboard
    }).filter('secondsToHHmmss', function($filter) {
        return function(seconds) {
            return $filter('date')(new Date(0, 0, 0).setSeconds(seconds), 'mm:ss');
        };
    })
    .controller('addNewQuiz', function($rootScope, $scope, $http, $stateParams, $state, $ionicModal) {
        $scope.currentState = $state.current.name;
        $scope.token = localStorage['token'];
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
            $scope.currentState = toState.name;
            $scope.token = localStorage['token'];
        })
        //Get the quiz info
        $http({
                method:"GET",
                headers:{
                    "code":"scteam",
                    "token":$scope.token
                },
                url: $rootScope.URL+'quiz/'+$stateParams.id,
                params: {},
                data: {}
            }).then(function(result) {
            $scope.quizDetails = result.data.info[0];
            $scope.quizData = result.data;
            console.log($scope.quizData);

        })

        //add questions button
        $scope.addQuestions = function(){
            console.log("INSERTED QUIZ ID: "+ $scope.quizDetails.id);
            $ionicModal.fromTemplateUrl('templates/quiz/question.html', {
              scope: $scope,
              animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.form = {};
                $scope.addquestionsModal = modal;

                $scope.table = { value: [] };
                $scope.addFormField = function() {
                    console.log("Add answers");
                    $scope.table.value.push('');
                }
                $scope.addquestionsModal.show();
            });
        }

        $scope.removeFormField = function(index) {
            if (index > -1) {
                $scope.table.value.splice(index, 1);
            }
        }

        //save questions
        $scope.saveQuestions = function(form){

            form.quiz = $scope.quizDetails.id;
            form.type = 2;

            if($scope.questionId){
                console.log("Exists", $scope.questionId);
                $scope.editForm = {
                    id: $scope.questionId,
                    quiz: $scope.quizDetails.id,
                    name: form.name,
                    type: 2
                }
                //update questions
                $http({
                    method:"PUT",
                    headers:{
                        "code":"scteam",
                        "token":$scope.token,
                        "content-type":"application/json"
                    },
                    url: $rootScope.URL+'quiz/question/'+$scope.questionId,
                    params: {},
                    data: $scope.editForm
                }).then(function(result) {
                    $scope.list = result.data;

                    //update answers
                    var i = 0;
                    var length = $scope.table.value.length;
                    $scope.updateAnswers = [];
                    $scope.newArray = [];
                    var k = 0;
                    angular.forEach($scope.quizData.answers, function(value, key){

                        if(value.quiz_question == $scope.questionId){
                            $scope.answerId = value.id;
                            $scope.newArray[k] = value;
                            k++;
                        }
                    });

                    var j = 0;

                    for(var i in $scope.newArray){
                        $scope.newArray[i].value = $scope.table.value[j];
                        if($scope.table.correct == $scope.table.value[j]){
                           $scope.newArray[i].correct = true;
                        }else{
                           $scope.newArray[i].correct = false;
                        }
                        j++;
                    }
                    $scope.updateAnswers = [];
                    angular.forEach($scope.newArray, function(value, key){
                        $scope.updateAnswers = {
                            id: value.id,
                            quiz_question: value.quiz_question,
                            value: value.value,
                            correct: value.correct
                        }
                        $http({
                            method:"PUT",
                            headers:{
                                "code":"scteam",
                                "token":$scope.token,
                                "content-type":"application/json"
                            },
                            url: $rootScope.URL+'quiz/answer/'+value.id,
                            params: {},
                            data: $scope.updateAnswers
                        }).then(function(result) {
                             $scope.addquestionsModal.hide();
                            $state.go($state.current, {}, { reload: true });
                        }, function(error) {
                            console.log("error");
                        });
                    });
                    //end of update answers

                    // $scope.addquestionsModal.hide();
                    // $state.go($state.current, {}, { reload: true });
                }, function(error) {
                    console.log(error);
                });


            }else{
                console.log("New NEW");
                $http({
                        method:"POST",
                        headers:{
                            "code":"scteam",
                            "token":$scope.token,
                            "content-type":"application/json"
                        },
                        url: $rootScope.URL+'quiz/question',
                        params: {},
                        data: form
                    }).then(function(result) {
                        console.log(result);
                        var count = 0;
                        form.quiz_question = result.data.insertId;
                        var i = 0;
                        var length = $scope.table.value.length;
                        loop();
                        function loop(){
                            if ($scope.table.value[i] == $scope.table.correct) {
                                correct = true
                            } else {
                                correct = false
                            }
                            form = {
                                quiz_question: form.quiz_question,
                                value: $scope.table.value[i],
                                correct: correct
                            }
                            $http({
                                method:"POST",
                                headers:{
                                    "code":"scteam",
                                    "token":$scope.token,
                                    "content-type":"application/json"
                                },
                                url: $rootScope.URL+'quiz/answer',
                                params: {},
                                data: form
                            }).then(function(result) {
                                console.log(result);
                                i++;
                                count++;
                                if (count == $scope.table.value.length) {
                                    console.log('Do post-submit stuff');
                                     $scope.addquestionsModal.hide();
                                     $state.go($state.current, {}, { reload: true });
                                } else {
                                    loop();
                                }
                            }, function(error) {
                                console.log(error);
                            });
                        }

                }, function(error) {
                    console.log(error);
                });

            }   //end of else


        } //end of save questions

        //delete a question
        $scope.removeQuestion = function(id) {
            var questionId = $scope.quizData.questions[id].id;
            $http({
                method:"DELETE",
                headers:{
                    "code":"scteam",
                    "token":$scope.token,
                    "content-type":"application/json"
                },
                url: $rootScope.URL+ 'quiz/question/'+questionId,
                params: {},
                data: {id: questionId}
            }).then(function(res) {
                $state.go($state.current, {}, { reload: true });
                console.log("Questions deleted successfully");
            }, function(error) {
                console.log(error);
            });
        } //end of delete question

        //finalize quiz
        $scope.finalizeQuiz = function(quizId){
            $scope.updateQuizform = {
                    name: $scope.quizDetails.name,
                    visible : 1,
                    published: 0
            }
            $http({
                method:"PUT",
                headers:{
                    "code":"scteam",
                    "token":$scope.token,
                    "content-type":"application/json"
                },
                url: $rootScope.URL+'quiz/'+quizId,
                params: {},
                data: $scope.updateQuizform
            }).then(function(result) {
                console.log("quiz finalized");
                $state.go($state.current, {}, { reload: true });
            }, function(error) {
                console.log(error);
            });
        }

        //publish quiz
        $scope.publishQuiz = function(quizId, status){
            if(status == 0){
                status = 1;
            }else{
                status = 0;
            }
            $scope.publishQuizform = {
                    name: $scope.quizDetails.name,
                    visible : 1,
                    published: status
            }
            $http({
                method:"PUT",
                headers:{
                    "code":"scteam",
                    "token":$scope.token,
                    "content-type":"application/json"
                },
                url: $rootScope.URL+'quiz/'+quizId,
                params: {},
                data: $scope.publishQuizform
            }).then(function(result) {
                    console.log("quiz published!!");

                    $state.go($state.current, {}, { reload: true });
                }, function(error) {
                    console.log(error);
            });
        }

        //close modal
        $scope.closeQuestionModal = function(){
            $scope.addquestionsModal.hide();
        }
        $scope.editQuestion = function(questionId){

            $ionicModal.fromTemplateUrl('templates/quiz/question.html', {
              scope: $scope,
              animation: 'slide-in-up'
            }).then(function(modal) {
              $scope.addquestionsModal = modal;

              $scope.form = [];
              $scope.table = { value: [] };
                angular.forEach($scope.quizData.questions, function(value, key){
                    if(value.id == questionId){
                        $scope.form.name = value.name;
                         $scope.questionId = questionId;
                    }
                });
                var i=0;
                angular.forEach($scope.quizData.answers, function(value, key){

                    if(value.quiz_question == questionId){
                        $scope.table.value[i] = value.value;
                        if(value.correct){
                            $scope.table.correct = value.value;
                        }
                        i++;
                    }
                });
                console.log("TABLE",$scope.table);
                $scope.addquestionsModal.show();
            });

        }
        $scope.saveQuiz = function(details){
            $http({
                method:"PUT",
                headers:{
                    "code":"scteam",
                    "token":$scope.token,
                    "content-type":"application/json"
                },
                url: $rootScope.URL+'quiz/'+details.id,
                params: {},
                data: details
            }).then(function(result) {
                    console.log("quiz updated!!");

                    $state.go($state.current, {}, { reload: true });
                }, function(error) {
                    console.log(error);
            });
        }

    })
