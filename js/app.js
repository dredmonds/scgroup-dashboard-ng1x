angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'datePicker', 'ngFileUpload', 'ngTableToCsv', 'ion-datetime-picker'])

.run(function($ionicPlatform, $state, $rootScope, $http) {


     function init () {

       if(localStorage['user'] && localStorage['token'] && localStorage['URL'] && localStorage['server']) {
         $rootScope.URL = localStorage['URL'];
         $rootScope.server = JSON.parse(localStorage['server']);
         $rootScope.currentUser = JSON.parse(localStorage['user']);
         // checking if user is from staffconnect and then to set them as an admin
         var idx = JSON.parse(localStorage['user']).username.lastIndexOf('@');
         $rootScope.showAdmin = (idx > -1 && JSON.parse(localStorage['user']).username.slice(idx+1) === 'staffconnectapp.com');
       } else if(localStorage['URL'] && localStorage['server']){
         $rootScope.URL = localStorage['URL'];
         $rootScope.server = JSON.parse(localStorage['server']);
         $state.go('login');
       } else {
         localStorage.clear();
         $state.go('pin');
       }

     }

     init();

     $rootScope.go = $state.go;

     $rootScope.refreshConfig = function() {
        if (localStorage['server']) {
             $rootScope.server = JSON.parse(localStorage['server']);
            $http.get("https://admin.staffconnectapp.com/api/" + $rootScope.server.pin)
                .then(function(result) {
                    $rootScope.URL = result.data;
                    localStorage['URL'] = result.data;
                    $http.get($rootScope.URL + 'config/' + $rootScope.server.pin)
                     .then(function(result) {
                         localStorage['server'] = JSON.stringify(result.data);
                         $rootScope.server = result.data;
                         $rootScope.config = JSON.parse(result.data.legacy);
                     }, function(error) {
                         console.log(error);
                     })
                }, function(error) {
                    $ionicLoading.hide();
                    if(error.status != 401 && error.status != 404) {
                        $ionicPopup.alert({
                            title: 'Error',
                            template: error.data,
                            okType: 'button-dark'
                        });
                    }
                })
        }

         
             
     }
     $rootScope.refreshConfig();

})
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.views.transition('none');
    $httpProvider.interceptors.push('authInterceptor');
    $stateProvider
        .state('pin', {
            url: '/pin',
            templateUrl: 'templates/pin.html',
            controller: 'pin'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'login'
        })
        .state('dashboard', {
            url: '/dashboard',
            abstract: true,
            templateUrl: 'templates/dashboard.html',
            controller: 'dashboard'
        })
        .state('dashboard.home', {
            url: '/home',
            views: {
                'content': {
                    templateUrl: 'modules/home/HomeView.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            }
        })
        .state('dashboard.userManagement', {
            url: '/userManagement',
            views: {
                'content': {
                    templateUrl: 'templates/userManagement/userManagement.html',
                    controller: 'userManagement'
                }
            }
        })
        .state('dashboard.userManagement-advanced', {
            url: '/userManagement/advanced',
            views: {
                'content': {
                    templateUrl: 'templates/userManagement/advanced.html',
                    controller: 'userManagement'
                }
            }
        })
        .state('dashboard.bulkAddUser', {
            url: '/bulkAddUser',
            views: {
                'content': {
                    templateUrl: 'templates/userManagement/bulkAddUser.html',
                    controller: 'bulkAddUser'
                }
            }
        })
        .state('dashboard.auth2', {
            url: '/auth2',
            views: {
                'content': {
                    templateUrl: 'templates/userManagement/auth2.html',
                    controller: 'auth2'
                }
            }
        })
        .state('dashboard.permissions', {
            url: '/permissions',
            views: {
                'content': {
                    templateUrl: 'templates/permissions/permissions.html',
                    controller: 'permissions'
                }
            }
        })
        .state('dashboard.community', {
            url: '/:featureIndex/community/:type',
            views: {
                'content': {
                    templateUrl: 'modules/communities/CommunitiesListView.html',
                    controller: 'CommunitiesListController'
                }
            }
        })
        .state('dashboard.community-detail', {
            url: '/:featureIndex/community/:type/:id',
            views: {
                'content': {
                    templateUrl: 'modules/communities/CommunitiesDetailView.html',
                    controller: 'CommunitiesDetailController'
                }
            }
        })
        .state('dashboard.clients', {
            url: '/clients',
            views: {
                'content': {
                    templateUrl: 'templates/clients/clients.html',
                    controller: 'clients'
                }
            }
        })
        .state('dashboard.client', {
            url: '/client/:id',
            views: {
                'content': {
                    templateUrl: 'templates/clients/client.html',
                    controller: 'client'
                }
            }
        })
        .state('dashboard.quiz', {
            url: '/:featureIndex/quiz/:id',
            views: {
                'content': {
                    templateUrl: 'templates/quiz/quiz.html',
                    controller: 'quiz'
                }
            }
        })
        .state('dashboard.quiz-detail', {
            url: '/:featureIndex/quiz-detail/:id',
            views: {
                'content': {
                    templateUrl: 'templates/quiz/add.html',
                    controller: 'addNewQuiz'
                }
            }
        })

        .state('dashboard.library', {
            url: '/:featureIndex/library/:id',
            views: {
                'content': {
                    templateUrl: 'modules/library/LibraryView.html',
                    controller: 'LibraryController',
                    controllerAs: 'vm'
                }
            }
        })

        .state('dashboard.rssfeed', {
            url: '/:featureIndex/rssfeed/:id',
            views: {
                'content': {
                    templateUrl: 'modules/rssfeed/RSSFeedView.html',
                    controller: 'RSSFeedController',
                    controllerAs: 'vm'
                }
            }
        })


        .state('dashboard.events', {
            url: '/:featureIndex/events/:id',
            views: {
                'content': {
                    templateUrl: 'modules/events/EventsView.html',
                    controller: 'EventsController',
                    controllerAs: 'vm'
                }
            }
        })

        .state('dashboard.questions', {
            url: '/:featureIndex/questions/:id',
            views: {
                'content': {
                    templateUrl: 'modules/questions/QuestionsView.html',
                    controller: 'QuestionsController',
                    controllerAs: 'vm'
                }
            }
        })

        .state('dashboard.survey', {
            url: '/:featureIndex/survey/:id',
            views: {
                'content': {
                    templateUrl: 'modules/survey/SurveyView.html',
                    controller: 'SurveyController',
                    controllerAs: 'vm'
                }
            }
        })

        .state('dashboard.survey-detail', {
            url: '/:featureIndex/survey/:id/detail/:surveyId',
            views: {
                'content': {
                    templateUrl: 'modules/survey/SurveyDetailView.html',
                    controller: 'SurveyDetailController',
                    controllerAs: 'vm'
                }
            }
        })

        .state('dashboard.survey-analytics', {
            url: '/:featureIndex/survey/:id/analytics/:surveyId',
            views: {
                'content': {
                    templateUrl: 'modules/survey/SurveyAnalyticsView.html',
                    controller: 'SurveyAnalyticsController',
                    controllerAs: 'vm'
                }
            }
        })

        .state('dashboard.analytics', {
            url: '/analytics',
            views: {
                'content': {
                    templateUrl: 'modules/analytics/AnalyticsView.html',
                    controller: 'AnalyticsController',
                    controllerAs: 'vm'
                }
            }
        })

    $urlRouterProvider.otherwise('/pin');
})

.factory('authInterceptor', function ($q, $rootScope) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if (localStorage['token']) {
                config.headers.token = localStorage['token'];
            }
            if ($rootScope.server) config.headers.code = $rootScope.server.pin;
            return config;
        },

        responseError: function(response) {
            if(response.status === 401 || response.status === 404) {
                $rootScope.$broadcast("signout");
                return $q.reject(response);
            } else {
                return $q.reject(response);
            }
        }
    };
})
.filter('fixDate', function() {
  return function(input) {
    return new moment(input).format('DD-MM-YYYY HH:mm');
  };
})

.service('LoginService', function( $http, $q, $rootScope, $state ) {

	// Return public API.
	return({
	    resetPassword:resetPassword,
	});

	function resetPassword(userObject) {
	    var request = $http({
	        method: "post",
	        url: $rootScope.URL+"auth/forgotpass",
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});
