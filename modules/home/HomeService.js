angular.module('starter.services', [])

.service('HomeService', function( $http, $q, $rootScope) {


	// Return public API.
	return({
	    getTicker: getTicker,
	    updateTicker: updateTicker,
	    sendPush: sendPush,
			getPush: getPush,
			getDevices: getDevices
	});


	function getTicker() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: $rootScope.URL + "ticker",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );

	}

	function updateTicker(item) {
		console.log(item)
	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"],
	        	"content-type":"application/json"
	        },
	        url: $rootScope.URL + "ticker",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );

	}

	function getPush() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: $rootScope.URL + "push/log",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );

	}

	function getDevices() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: $rootScope.URL + "push/devices",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );

	}

	function sendPush(item) {
		console.log(item)
	    var request = $http({
	        method: "POST",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: $rootScope.URL + "push/send",
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );

	}

	function handleError( response ) {

	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );

	}

	function handleSuccess( response ) {

	    return( response );

	}


});
