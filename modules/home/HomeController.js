angular.module('starter.controllers')

.controller('HomeController', function(HomeService, $scope, $state, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {

	var vm = this;

	vm.updateTicker = confirmUpdateTicker;
	vm.sendPush = confirmSendPush;


	vm.ticker = {"value":""};
	vm.push = {"message":""};

	getTicker();

    function getTicker () {

	    HomeService.getTicker()
	        .then(function(success){
            if(success.data.value){
	        		vm.ticker = success.data;
	        	} else {
	        		vm.ticker.value = "";
	        	}
	        }, function(error){
	          console.log(error);
	        })

	}

	getPush();

    function getPush () {

	    HomeService.getPush()
	        .then(function(success){
            vm.pushLog = success.data;
	        }, function(error){
	          console.log(error);
	        })

	}

	getDevices();

    function getDevices () {

	    HomeService.getDevices()
	        .then(function(success){
						console.log(success);
            vm.devices = success.data.devices;
	        }, function(error){
	          console.log(error);
	        })

	}

    function confirmUpdateTicker (item) {

		$ionicPopup.confirm({
			title: 'Update Ticker',
			template: 'Are you sure you want to update the ticker tape?'
		}).then(function(res) {
			if(res) updateTicker(item);
		});

	}

    function updateTicker (item) {

	    HomeService.updateTicker(item)
	        .then(function(success){
	            getTicker()
	        }, function(error){
	          console.log(error);
	        })
	}

	function confirmSendPush (item) {
		if(item.message.length > 4) {
			$ionicPopup.confirm({
				title: 'Send Push',
				template: 'Are you sure you want to send this message to all users of the app around the world?'
			}).then(function(res) {
				if(res) sendPush(item);
			});
		}
	}

    function sendPush (item) {

	    HomeService.sendPush(item)
	        .then(function(success){
						getPush();
				$ionicPopup.alert({
					title: 'Push Sent',
					template: 'Your message has been sent to all users of the app.'
				})
				vm.push = {"message":""};
	        }, function(error){
	          console.log(error);
	        })
	}



});
