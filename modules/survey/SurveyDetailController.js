

angular.module('starter.controllers')

.controller('SurveyDetailController', function(SurveyService, $scope, $state, $stateParams, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {

	var vm = this;

	// vm.feature = $scope.config.features[$stateParams.featureIndex];
	vm.survey = {};
	vm.questions = {};
	vm.surveyForm = {};

	vm.currentDate = new Date().toJSON();

	vm.updateSurvey = updateSurvey;
	vm.finalizeSurvey = finalizeSurvey;
	vm.publishSurvey = publishSurvey;
	vm.confirmRemoveSurvey = confirmRemoveSurvey;

	vm.showNewQuestionModal = addQuestionModal;
	vm.showEditQuestionModal = editQuestionModal;
	vm.closeQuestionModal = closeQuestionModal;
	vm.addQuestion = addQuestion;
	vm.updateQuestion = updateQuestion;
	vm.confirmRemoveQuestion = confirmRemoveQuestion;

	vm.removeAnswer = removeAnswer;

	getSurvey();

    function getSurvey () {

	    SurveyService.getSurvey($stateParams.surveyId)
	        .then(function(success){

	        	vm.survey = success.data.info[0];
	        	vm.surveyForm = angular.copy(vm.survey);
	        	console.log(vm.survey)
	        	for (var i = 0; i < success.data.answers.length; i++) {
	        		for (var x = 0; x < success.data.questions.length; x++) {
	        			if(success.data.answers[i].quiz_question == success.data.questions[x].id){

	        				if(!success.data.questions[x].answers){
	        					success.data.questions[x].answers = [];
	        				}
	        				success.data.questions[x].answers.push(success.data.answers[i]);
	        			}
	        		}
	        	}

	        	vm.questions = success.data.questions;
	        	console.log(vm.questions);

	        }, function(error){
	          console.log(error);
	        })

	}

	function updateSurvey (item) {
		$ionicLoading.show();
		if(item.name){
		    SurveyService.updateSurvey(item)
		        .then(function(success){
		        	console.log(success)
		        	getSurvey();
		        	$ionicLoading.hide();
		        }, function(error){
		          console.log(error);
		        })
		}

	}

	function finalizeSurvey (item) {
		$ionicPopup.confirm({
			title: 'Finalize Survey',
			template: 'Questions cannot be added or edited once you finalize a survey. Are you sure you want to finalize this survey?'
		}).then(function(res) {
			if(res) {
				item.visible = 1;
				updateSurvey(item);
			}
		});
	}

	function publishSurvey (item) {
		item.published = !item.published;
		updateSurvey(item);
	}

    function confirmRemoveSurvey (item) {

		$ionicPopup.confirm({
			title: 'Delete Survey',
			template: 'Are you sure you want to delete this survey?'
		}).then(function(res) {
			if(res) removeSurvey(item);
		});

	}

    function removeSurvey (item) {
	    SurveyService.removeSurvey(item.id)
	        .then(function(success){
	        	$state.go("dashboard.survey");
	        }, function(error){
	          console.log(error);
	        })
	}

	function addQuestionModal () {

        $ionicModal.fromTemplateUrl('modules/survey/NewQuestionModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up'
        }).then(function(modal) {
        	vm.questionForm = {};
        	vm.questionForm.answers = [{},{}]
            vm.addQuestionModal = modal;
            vm.addQuestionModal.show();
        });

	}

	function editQuestionModal (item) {

        $ionicModal.fromTemplateUrl('modules/survey/EditQuestionModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up'
        }).then(function(modal) {
        	vm.questionForm = angular.copy(item);
            vm.addQuestionModal = modal;
            vm.addQuestionModal.show();
        });

	}

	function closeQuestionModal () {
		vm.addQuestionModal.remove();
	}

	function addQuestion (item) {
		item.quiz = vm.survey.id;
		var type = item.type;

		if(item && item.name && item.type) {
		    SurveyService.addQuestion(item)
		        .then(function(success){
		        	if(type != 1) {
		        		addAnswers(item.answers, success.data.insertId);
		        	} else {
		        		getSurvey();
		        		vm.closeQuestionModal();
		        	}

		        }, function(error){
		          console.log(error);
		        })
	    } else {
			$ionicPopup.alert({
				title: 'Missing fields',
				template: 'Please ensure all fields have been entered.'
			}).then(function(res) {
				// if(res) removeEvent(item);
			});
	    }
	}

    function updateQuestion (item) {
	    SurveyService.updateQuestion(item)
	        .then(function(success){
            if(item.type != 1){
                updateAnswers(item.answers);
            } else {
              getSurvey();
              vm.closeQuestionModal();
            }
	        }, function(error){
	          console.log(error);
	        })
	  }

    function confirmRemoveQuestion (item) {

		$ionicPopup.confirm({
			title: 'Delete Question',
			template: 'Are you sure you want to delete this question?'
		}).then(function(res) {
			if(res) removeQuestion(item);
		});

	}

    function removeQuestion (item) {
	    SurveyService.removeQuestion(item.id)
	        .then(function(success){
	            getSurvey();
	        }, function(error){
	          console.log(error);
	        })
	}

	function addAnswers (answers, questionId) {
		var length = answers.length-1;
		var i = 0;
		addAnswer();
		function addAnswer() {

				answers[i].quiz_question = questionId;
				item = answers[i]
		    SurveyService.addAnswer(item)
		        .then(function(success){
							if(i < length) {
								i++;
								addAnswer();
							} else {
								getSurvey();
								vm.closeQuestionModal();
							}
		        }, function(error){
		          console.log(error);
		        })
		}

	}



	function updateAnswers (answers, questionId) {
		for (var i = 0; i < answers.length; i++) {
			updateAnswer(answers[i]);
		}

	}

	function updateAnswer (item) {
	    SurveyService.updateAnswer(item)
	        .then(function(success){
	            getSurvey();
	            vm.closeQuestionModal();
	        }, function(error){
	          console.log(error);
	        })
	}

	function removeAnswer (index) {
		vm.questionForm.answers.splice(index, 1);
	    // SurveyService.removeAnswer(item.id)
	    //     .then(function(success){
	    //         getSurvey();
	    //     }, function(error){
	    //       console.log(error);
	    //     })
	}


});
