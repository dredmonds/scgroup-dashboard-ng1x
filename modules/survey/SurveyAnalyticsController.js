

angular.module('starter.controllers')

.controller('SurveyAnalyticsController', function(SurveyService, $scope, $state, $stateParams, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {

	var vm = this;

	// vm.feature = $scope.config.features[$stateParams.featureIndex];
	vm.currentDate = new Date().toJSON();
	
	getSurvey();

	var questions = {};

    function getSurvey () {

	    SurveyService.getSurvey($stateParams.surveyId)
	        .then(function(success){

	        	vm.survey = success.data.info[0];
	        	vm.surveyForm = angular.copy(vm.survey);
	        	for (var x = 0; x < success.data.questions.length; x++) {
	        		for (var i = 0; i < success.data.answers.length; i++) {
	        		
	        			if(success.data.answers[i].quiz_question == success.data.questions[x].id){

	        				if(!success.data.questions[x].answers){
	        					success.data.questions[x].answers = {};
	        				}

							success.data.answers[i].count = 0;
	        				success.data.questions[x].answers[success.data.answers[i].id] = success.data.answers[i];
	        				questions[success.data.questions[x].id] = success.data.questions[x];
	        			}
	        		}
	        		if(success.data.questions[x].type == "1"){
	        			questions[success.data.questions[x].id] = success.data.questions[x];
	        		}
	        	}
	        	vm.questions = questions;
	        	console.log(vm.questions)
	        	getAnalytics();
	        }, function(error){
	          console.log(error);
	        })

	}


    function getAnalytics () {
    	
	    SurveyService.getAnalytics($stateParams.surveyId)
	        .then(function(success){
	        	for (var i = 0; i < success.data.length; i++) {
	        		var answers = JSON.parse(success.data[i].answer);
	        		for (var x = answers.length - 1; x >= 0; x--) {
	        			if(vm.questions[answers[x].question] && vm.questions[answers[x].question].type == 1) {
	        				if(!vm.questions[answers[x].question].answers || !vm.questions[answers[x].question].answers.length) {
	        					vm.questions[answers[x].question].answers = [];
	        				}
	        				vm.questions[answers[x].question].answers.push({value:answers[x].answer});
	        			} else if(vm.questions[answers[x].question]){
	        				if(typeof answers[x].answer == 'string'){
	        					answers[x].answer = [answers[x].answer];
	        				}
	        				for (var y = answers[x].answer.length - 1; y >= 0; y--) {
	        					// console.log(vm.questions[answers[x].question].answers[answers[x].answer[y]])
	        					if(vm.questions[answers[x].question].answers[answers[x].answer[y]]){
	        						vm.questions[answers[x].question].answers[answers[x].answer[y]].count++;
	        					}
	        				}
	        			}
	        		}
	        	}

	        	vm.analytics = success.data;

	        }, function(error){
	          console.log(error);
	        })

	}

})