

angular.module('starter.controllers')

.controller('SurveyController', function(SurveyService, $scope, $state, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {
      
	var vm = this;

	// vm.feature = $scope.config.features[$stateParams.featureIndex];
	vm.surveyItems = [];
	vm.newSurveyForm = {};

	vm.currentDate = new Date().toJSON();

	vm.addSurvey = addSurvey;

	vm.showSurveyModal = addSurveyModal;
	vm.showEditSurveyModal = editSurveyModal;
	vm.closeSurveyModal = closeSurveyModal;
	vm.addSurvey = addSurvey;
	vm.updateSurvey = updateSurvey;
	vm.confirmRemoveSurvey = confirmRemoveSurvey;
	
	getSurveys();

    function getSurveys () {
    	vm.surveyItems = [];
	    SurveyService.getSurveys()
	        .then(function(success){
	        	for (var i = 0; i < success.data.length; i++) {
	        		if(success.data[i].type == 1) {
	        			vm.surveyItems.push(success.data[i]);
	        		}
	        	}

	        }, function(error){
	          console.log(error);
	        })

	}

	function addSurvey (item) {

		if(item.name){
		    SurveyService.addSurvey(item)
		        .then(function(success){
		        	vm.newSurveyForm = {};
		        	getSurveys();
		        }, function(error){
		          console.log(error);
		        })
		}

	}

	function addSurveyModal () {

        $ionicModal.fromTemplateUrl('modules/survey/SurveyNewItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up'
        }).then(function(modal) {
        	vm.newSurveyForm = {};
            vm.addSurveyModal = modal;
            vm.addSurveyModal.show();
        }); 

	}

	function getQuestions () {

	}

	function editSurveyModal (item) {

        $ionicModal.fromTemplateUrl('modules/survey/SurveyEditItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up'	
        }).then(function(modal) {
        	vm.newSurveyForm = angular.copy(item);
            vm.addSurveyModal = modal;
            vm.addSurveyModal.show();
        }); 

	}

	function closeSurveyModal () {
		vm.addSurveyModal.remove();
	}

	// function addSurvey (item) {
	// 	if(item && item.title && item.additionalNotes && item.startTime && item.endTime) {
	// 	    SurveyService.addSurvey(item)
	// 	        .then(function(success){
	// 	            getSurveys();
	// 	            vm.closeSurveyModal();
	// 	        }, function(error){
	// 	          console.log(error);
	// 	        })
	//     } else {
	// 		$ionicPopup.alert({
	// 			title: 'Missing fields',
	// 			template: 'Please ensure all fields have been entered.'
	// 		}).then(function(res) {
	// 			// if(res) removeEvent(item);
	// 		});
	//     }
	// }

    function updateSurvey (item) {
	    SurveyService.updateSurvey(item)
	        .then(function(success){
	            getSurveys();
	            vm.closeSurveyModal();
	        }, function(error){
	          console.log(error);
	        })
	}

    function confirmRemoveSurvey (item) {

		$ionicPopup.confirm({
			title: 'Delete Survey',
			template: 'Are you sure you want to delete this survey?'
		}).then(function(res) {
			if(res) removeSurvey(item);
		});

	}

    function removeSurvey (item) {
	    SurveyService.removeSurvey(item.id)
	        .then(function(success){
	            getSurveys();
	        }, function(error){
	          console.log(error);
	        })
	}


})

.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return parseInt(val, 10);
      });
      ngModel.$formatters.push(function(val) {
        return '' + val;
      });
    }
  };
});