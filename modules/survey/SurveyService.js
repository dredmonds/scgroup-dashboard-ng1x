angular.module('starter.services')
	
.service('SurveyService', function( $http, $q, $rootScope) {

	var host = $rootScope.URL;

	// Return public API.
	return({
	    getSurveys: getSurveys,
	    getSurvey:getSurvey,
	    addSurvey: addSurvey,
	    updateSurvey: updateSurvey,
	    removeSurvey: removeSurvey,
	    addQuestion: addQuestion,
	    updateQuestion: updateQuestion,
	    removeQuestion: removeQuestion,
	    addAnswer: addAnswer,
	    updateAnswer: updateAnswer,
	    removeAnswer: removeAnswer,
	    getAnalytics: getAnalytics
	});


	function getSurveys() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/all",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getSurvey(id) {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addSurvey(item) {
		item.type = 1;
	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateSurvey(item) {

	    var request = $http({
	        method: "PUT",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/"+item.id,
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function removeSurvey(id) {

	    var request = $http({
	        method: "DELETE",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/"+id,
	        params: {},
	        data: {id:id},
			processData: false,
			contentType: false
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addQuestion(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/question",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateQuestion(item) {

	    var request = $http({
	        method: "PUT",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/question/"+item.id,
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function removeQuestion(id) {
	    var request = $http({
	        method: "DELETE",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/question/"+id,
	        data: {id:id}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addAnswer(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/answer",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateAnswer(item) {
	    var request = $http({
	        method: "PUT",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/answer/"+item.id,
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function removeAnswer(item) {
	    var request = $http({
	        method: "DELETE",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/answer/"+item.id
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getAnalytics(id) {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"quiz/analytics/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});