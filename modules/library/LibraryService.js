angular.module('starter.services')
	
.service('LibraryService', function( $http, $q, $rootScope) {

	var host = $rootScope.URL;

	// Return public API.
	return({
	    getDocuments: getDocuments,
	    addDocument: addDocument,
	    updateDocument: updateDocument,
	    removeDocument: removeDocument,
	    getCategories: getCategories,
	    addCategory: addCategory,
	    updateCategory: updateCategory,
	    removeCategory: removeCategory
	});


	function getDocuments() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addDocument(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library-new-item",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateDocument(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library-update-item",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function removeDocument(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library-delete-item",
	        params: {},
	        data: {id:item._id}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getCategories() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library-categories",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addCategory (item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library-categories-new-item",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateCategory (item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library-categories-update-item",
	        params: {},
	        data: {id:item._id}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function removeCategory(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"library-categories-delete-item",
	        params: {},
	        data: {id:item._id}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});