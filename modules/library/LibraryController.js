

angular.module('starter.controllers')

.controller('LibraryController', function(LibraryService, $scope, $state, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {

	var vm = this;

	// vm.feature = $scope.config.features[$stateParams.featureIndex];
	vm.libraryItems = [];
	vm.libraryCategories = [];

	vm.showDocumentModal = addDocumentModal;
	vm.showEditDocumentModal = editDocumentModal;
	vm.closeDocumentModal = closeDocumentModal;
	vm.addDocument = uploadDocument;
	vm.updateDocument = updateDocument;
	vm.confirmRemoveDocument = confirmRemoveDocument;
	vm.addCategory = addCategory;
	vm.updateCategory = updateCategory;
	vm.removeCategory = confirmRemoveCategory;

	getDocuments();
	getCategories();

    function getDocuments () {

	    LibraryService.getDocuments()
	        .then(function(success){
	            vm.libraryItems = success.data;
	        }, function(error){
	          console.log(error);
	        })

	}

	function addDocumentModal () {

        $ionicModal.fromTemplateUrl('modules/library/LibraryNewItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up',
          	backdropClickToClose: false
        }).then(function(modal) {
            vm.addDocumentModal = modal;
            vm.addDocumentModal.show();
        });

	}

	function editDocumentModal (item) {

        $ionicModal.fromTemplateUrl('modules/library/LibraryEditItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up',
          	backdropClickToClose: false
        }).then(function(modal) {
        	vm.documentForm = angular.copy(item);
            vm.addDocumentModal = modal;
            vm.addDocumentModal.show();
        });

	}

	function closeDocumentModal () {
		vm.addDocumentModal.remove();
    vm.documentForm = {};
	}

    function uploadDocument (item) {
    	if(item && item.title && item.file && item.category) {
			$ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner><br />0%'});
			Upload.upload({
				url: $rootScope.URL + 'upload',
				data: {file: item.file}
			}).then(function (result) {
				$ionicLoading.hide();
				console.log(result.data[0].filename)
				item.libraryDocument = result.data[0].filename;
				item.fileExtension = result.data[0].extension;
				addDocument(item);
			}, function (resp) {
				$ionicLoading.hide();
				console.log(resp);
			}, function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				$ionicLoading.show({template: '<i class="icon ion-upload"></i><br />' + progressPercentage + '%'});
			});
		}

	}

	function addDocument (item) {
	    LibraryService.addDocument(item)
	        .then(function(success){
	            getDocuments();
	            vm.closeDocumentModal();
	        }, function(error){
	          console.log(error);
	        })
	}

    function updateDocument (item) {
	    LibraryService.updateDocument(item)
	        .then(function(success){
	            getDocuments();
	            vm.closeDocumentModal();
	        }, function(error){
	          console.log(error);
	        })
	}

    function confirmRemoveDocument (item) {

		$ionicPopup.confirm({
			title: 'Delete Document',
			template: 'Are you sure you want to delete this document?'
		}).then(function(res) {
			if(res) removeDocument(item);
		});

	}

    function removeDocument (item) {
	    LibraryService.removeDocument(item)
	        .then(function(success){
	            getDocuments();
	            vm.closeDocumentModal();
	        }, function(error){
	          console.log(error);
	        })
	}

    function getCategories () {
	    LibraryService.getCategories()
	        .then(function(success){
	            vm.libraryCategories = success.data;
	        }, function(error){
	          console.log(error);
	        })
	}

    function addCategory (item) {
    	if(item.title && item.title.length > 0) {
		    LibraryService.addCategory(item)
		        .then(function(success){
		        	getCategories();
		        	vm.categoryForm = {};
		        }, function(error){
		          console.log(error);
		        })
    	}
	}

    function updateCategory () {
	    LibraryService.updateCategory()
	        .then(function(success){
	            vm.libraryItems = success.data;
	        }, function(error){
	          console.log(error);
	        })
	}

    function confirmRemoveCategory (item) {

		$ionicPopup.confirm({
			title: 'Delete Category',
			template: 'Are you sure you want to delete this category?'
		}).then(function(res) {
			if(res) removeCategory(item);
		});

	}

    function removeCategory (item) {

	    LibraryService.removeCategory(item)
	        .then(function(success){
	            getCategories()
	        }, function(error){
	          console.log(error);
	        })
	}



});
