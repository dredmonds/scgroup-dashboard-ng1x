

angular.module('starter.controllers')

.controller('AnalyticsController', function(AnalyticsService, $scope, $state, $http, $ionicPopup, $filter, $ionicModal, Upload, $ionicLoading, $rootScope, $timeout) {

    (function (w, d, s, g, js, fjs) {
        g = w.gapi || (w.gapi = {});
        g.analytics = {
            q: [],
            ready: function (cb) {
                this.q.push(cb)
            }
        };
        js = d.createElement(s);
        fjs = d.getElementsByTagName(s)[0];
        js.src = 'https://apis.google.com/js/platform.js';
        fjs.parentNode.insertBefore(js, fjs);
        js.onload = function () {
            g.load('analytics')
            getGAKey();
        };
    }(window, document, 'script'));

	var vm = this;

	vm.dateRange = {};

	var gaKey;
	var ids = JSON.parse(JSON.parse(localStorage['server']).legacy).global.googleAnalytics.viewId;



    function getGAKey () {
      AnalyticsService.getGAKey()
        .then(function(success){
          gaKey = success.data;
          init();
        }, function(error){
          console.log(error)
        })
    }


    function init() {

        gapi.analytics.auth.authorize({
          serverAuth: {
            access_token: gaKey
          }
        });

        var tendaysago = new Date();
        tendaysago.setDate(tendaysago.getDate() - 10);
        vm.dateRange.start = new Date(tendaysago);
        vm.dateRange.end = new Date();
        var fromDate = $filter('date')(new Date(tendaysago), 'yyyy-MM-dd');
        var toDate = $filter('date')(new Date(), 'yyyy-MM-dd');

        // Step 5: Create the timeline chart.
        var newUsers = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:newUsers',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            },
            chart: {
                type: 'LINE',
                container: 'newUsers',
                options: {
                    title: "New Users",
                    'animation.duration': 200,
                    legend: false,
                    animation: {
                        duration: 500,
                        startup: true
                    }
                }
            },
        });


        var activeUsers = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:users',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            },
            chart: {
                type: 'LINE',
                container: 'activeUsers',
                options: {
                    title: "Active Users",
                    'animation.duration': 200,
                    legend: false,
                    animation: {
                        duration: 500,
                        startup: true
                    }
                }
            },
        });

        var avgSessionDuration = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:avgSessionDuration',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            },
            chart: {
                type: 'LINE',
                container: 'avgSessionDuration',
                options: {
                    title: "Avg. Session Duration (seconds)",
                    'animation.duration': 200,
                    legend: false,
                    vAxis: {
                        format: 'short',
                    },
                    animation: {
                        duration: 500,
                        startup: true
                    },
                    NumberFormat: {
                        pattern: '$###,###'
                    }
                }
            },
        });

        var avgSessionDurationData = new gapi.analytics.report.Data({
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:avgSessionDuration',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            }
        });

        avgSessionDurationData.on('success', function (response) {
            console.log(response)
        });

        var screenviewsPerSession = new gapi.analytics.googleCharts.DataChart({
            reportType: 'ga',
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:screenviewsPerSession',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            },
            chart: {
                type: 'LINE',
                container: 'screenviewsPerSession',
                options: {
                    title: "Screen Views Per Session",
                    'animation.duration': 200,
                    legend: false,
                    vAxis: {
                        format: '0',
                    },
                    animation: {
                        duration: 500,
                        startup: true
                    }
                }
            },
        });


        var screens = new gapi.analytics.report.Data({
            query: {
                ids: ids,
                metrics: 'ga:screenviews',
                dimensions: 'ga:screenName',
                'start-date': fromDate,
                'end-date': toDate,
                'sort': '-ga:screenviews',
            }
        });


        screens.on('success', function (response) {
            $scope.screens = response.rows;
            $scope.$apply()
        });


        var cities = new gapi.analytics.report.Data({
            query: {
                ids: ids,
                metrics: 'ga:users',
                dimensions: 'ga:city',
                'start-date': fromDate,
                'end-date': toDate,
                'sort': '-ga:users',
            }
        });


        cities.on('success', function (response) {
            $scope.cities = response.rows;
            $scope.$apply()
        });


        var devices = new gapi.analytics.report.Data({
            query: {
                ids: ids,
                metrics: 'ga:sessions',
                dimensions: 'ga:mobileDeviceModel',
                'start-date': fromDate,
                'end-date': toDate,
            }
        });


        devices.on('success', function (response) {
            $scope.devices = response.rows;
            $scope.$apply()
        });


        var activeUsersNumber = new gapi.analytics.report.Data({
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:users',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            }
        });


        activeUsersNumber.on('success', function (response) {

            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            var firstDate = new Date(fromDate);
            var secondDate = new Date(toDate);

            var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
            diffDays++;
            if(diffDays > 0) {
                document.getElementById("activeUsersNumber").innerHTML = Math.round(response.totalsForAllResults['ga:users']/diffDays);
            } else {
                document.getElementById("activeUsersNumber").innerHTML = response.totalsForAllResults['ga:users'];
            }
            //document.getElementById("activeUsersNumber").innerHTML = response.rows;

        });


        var screenSessionNumber = new gapi.analytics.report.Data({
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:screenviewsPerSession',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            }
        });


        screenSessionNumber.on('success', function (response) {
            document.getElementById("screenSessionNumber").innerHTML = parseInt(response.totalsForAllResults['ga:screenviewsPerSession']);
            //document.getElementById("activeUsersNumber").innerHTML = response.rows;
        });


        var timeSessionNumber = new gapi.analytics.report.Data({
            query: {
                'dimensions': 'ga:date',
                'metrics': 'ga:avgSessionDuration',
                'start-date': fromDate,
                'end-date': toDate,
                'ids': ids
            }
        });


        timeSessionNumber.on('success', function (response) {
            document.getElementById("timeSessionNumber").innerHTML = response.totalsForAllResults['ga:avgSessionDuration'].toHHMMSS();
            //document.getElementById("activeUsersNumber").innerHTML = response.rows;
        });


        // Step 6: Hook up the components to work together.
        $timeout(function(){


            activeUsersNumber.execute();
            screenSessionNumber.execute();
            timeSessionNumber.execute();
            newUsers.execute();
            activeUsers.execute();
            avgSessionDuration.execute();
            //avgSessionDurationData.execute();
            screenviewsPerSession.execute();
            screens.execute();
            cities.execute();
            devices.execute();

            $timeout(function(){
                $scope.analyticsReady = true;
            },1000)
        },10);

        $scope.dateChange = function (data) {
        	console.log(vm.dateRange)

            if (data) {
                var tendaysago = new Date();
                tendaysago.setDate(tendaysago.getDate() - 10);
                vm.dateRange.start = new Date(tendaysago);
                vm.dateRange.end = new Date();
                fromDate = $filter('date')(new Date(tendaysago), 'yyyy-MM-dd');
                toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            } else {
                var fromDateDisplay = vm.dateRange.start;
                var toDateDisplay = vm.dateRange.end;

                fromDate = $filter('date')(new Date(fromDateDisplay), 'yyyy-MM-dd');
                toDate = $filter('date')(new Date(toDateDisplay), 'yyyy-MM-dd');

                $scope.minDate = new Date(fromDateDisplay);


                if(new Date(toDateDisplay) < new Date(fromDateDisplay)){
                    $scope.showDateError = true;
                    return
                } else {
                    $scope.showDateError = false;
                }

            }

            var dateRange = {'start-date': fromDate, 'end-date': toDate}

            activeUsersNumber.set({query: dateRange}).execute();
            screenSessionNumber.set({query: dateRange}).execute();
            timeSessionNumber.set({query: dateRange}).execute();
            newUsers.set({query: dateRange}).execute();
            activeUsers.set({query: dateRange}).execute();
            avgSessionDuration.set({query: dateRange}).execute();
            //avgSessionDurationData.set({query: dateRange}).execute();
            screenviewsPerSession.set({query: dateRange}).execute();
            screens.set({query: dateRange}).execute();
            cities.set({query: dateRange}).execute();
            devices.set({query: dateRange}).execute();

        };

    };

    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            if (toState.name === 'dashboard.analytics') {
                location.reload();
            }
        });

    String.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }

});
