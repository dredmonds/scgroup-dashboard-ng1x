angular.module('starter.services')
	
.service('AnalyticsService', function( $http, $q, $rootScope) {

	var host = $rootScope.URL;
	// Return public API.
	return({
	    getGAKey: getGAKey,
	    getProperties: getProperties,
	});


	function getGAKey() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: $rootScope.URL+"googleAuth",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function getProperties() {

		var key = "";
		this.getGAKey()
		.then(function(success){
			console.log(success)
			key = success;
			return( request.then( handleSuccess, handleError ) );
		}, function(error){
			return( request.then( handleSuccess, handleError ) );
		})

	    var request = $http({
	        method: "GET",
	        url: "https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties?key="+key,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});