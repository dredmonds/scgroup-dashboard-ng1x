

angular.module('starter.controllers')

.controller('CommunitiesListController', function($scope, $http, $state, $stateParams, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup) {

      $scope.loadCommunities($stateParams.type);

      $scope.newCommunity = {};

      $scope.type = $stateParams.type;

      $ionicModal.fromTemplateUrl('modules/communities/add.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: false
      }).then(function(modal) {
        $scope.addCommunityModal = modal;
      });
      $scope.openAddCommunityModal = function() {
        $scope.newCommunity = {};
        $scope.loadPermissions().then(function() {
          for (i in $scope.departments) {
            $scope.departments[i].permissions = 0;
          }
          $scope.name = '';
          $scope.addCommunityModal.show();
        });
      };
      $scope.closeAddCommunitytModal = function() {
        $scope.addCommunityModal.hide();
      };

      $scope.addCommunity = function(name, locations, departments, visible, moderation) {

        console.log(name, locations, departments, visible);

        if (!visible) {
          visible = false;
        }
        var perms = {locations: [], departments: []};
        for (i in locations) {
          if (locations[i].access) perms.locations.push(locations[i]._id);
        }
        for (i in departments) {
          if (departments[i].permissions && departments[i].permissions > 0) perms.departments.push({department: departments[i]._id, permissions: departments[i].permissions});
        }
        console.log(perms);
        //return false;
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br />Adding...'
        });
        $http.post($rootScope.URL + 'communities/' + $stateParams.type, {name: name, locations: perms.locations, departments: perms.departments, visible: visible, moderation: moderation})
          .then(function(result) {
            $scope.loadCommunities($stateParams.type);
            $scope.closeAddCommunitytModal();
            $ionicLoading.hide();
          }, function(error) {
            $ionicLoading.hide();
            if(error.status != 401) {
                $ionicPopup.alert({
                    title: 'Error',
                    template: error.data,
                    okType: 'button-dark'
                });
            }
          });

      }

})
