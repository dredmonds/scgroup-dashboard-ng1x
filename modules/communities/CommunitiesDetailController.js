angular.module('starter.controllers')

.controller('CommunitiesDetailController', function(CommunitiesService, $scope, $http, $state, $stateParams, $rootScope, $ionicModal, $sce, $ionicLoading, $ionicPopup, $q, Upload) {

        $scope.loadCommunities($stateParams.type).then(function() {
          for (i in $scope.communities) {
            if ($scope.communities[i]._id == $stateParams.id) {
              $scope.community = $scope.communities[i];
              $scope.community.posts = [];
              $scope.loadPosts();
            }
          }
        })

        $scope.loadPosts = function(restore) {
          // var deferred = $q.defer();
          $scope.users = [];
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
          });
          getPosts(restore);

        }

        function getPosts (restore) {

          if(restore) {
            var url = $rootScope.URL + 'posts/' + $scope.community._id + '?amount='+$scope.community.posts.length+'&videosReady=true';
          } else {
            var url = $rootScope.URL + 'posts/' + $scope.community._id + '?page='+$scope.pagePagination+'&videosReady=true';
          }

          $http.get(url)
            .then(function(result) {
              $ionicLoading.hide();

              if(restore){
                $scope.community.posts = result.data;
              } else if(result.data && result.data.length > 0) {

                for (var i = 0; i < result.data.length; i++) {
                  $scope.community.posts.push(result.data[i]);
                }

              } else {
                $scope.moreDataCanBeLoaded = false;
              }

              // $scope.$broadcast('scroll.resize');
              $scope.$broadcast('scroll.infiniteScrollComplete');
              // deferred.resolve();
            }, function(error) {
              $ionicLoading.hide();
              if(error.status != 401) {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: error.data,
                      okType: 'button-dark'
                  });
              }
              // deferred.reject();
            });
          // return deferred.promise;
        }

        $scope.moreDataCanBeLoaded = true;
        $scope.pagePagination = 0;
        $scope.loadMoreData = function () {
            console.log("load more", $scope.infiniteAmount)
            $scope.pagePagination++;
            getPosts($scope.infiniteAmount);
        };






        $ionicModal.fromTemplateUrl('modules/communities/edit.html', {
          scope: $scope,
          animation: 'slide-in-up',
          backdropClickToClose: false
        }).then(function(modal) {
          $scope.editCommunityModal = modal;
        });
        $scope.openEditCommunityModal = function() {
          $scope.loadPermissions().then(function() {
            for (i in $scope.locations) {
              for (x in $scope.community.locations) {
                if ($scope.community.locations[x] == $scope.locations[i]._id) {
                  $scope.locations[i].access = true;
                }
              }
            }
            for (i in $scope.departments) {
              $scope.departments[i].permissions = 0;
            }
            for (i in $scope.departments) {
              for (x in $scope.community.departments) {
                if ($scope.community.departments[x].department == $scope.departments[i]._id) {
                  $scope.departments[i].permissions = $scope.community.departments[x].permissions;
                }
              }
            }

            $scope.name = $scope.community.name;
            $scope.visible = $scope.community.visible;
            $scope.moderation = $scope.community.moderation;
            $scope.editCommunityModal.show();
          });
        };
        $scope.closeEditCommunitytModal = function() {
          $scope.editCommunityModal.hide();
        };

        $scope.editCommunity = function(name, locations, departments, visible, moderation) {
          if (!visible) {
            visible = false;
          }
          var posts = $scope.community.posts;

          var perms = {locations: [], departments: []};
          for (i in locations) {
            if (locations[i].access) perms.locations.push(locations[i]._id);
          }
          for (i in departments) {
            if (departments[i].permissions && departments[i].permissions > 0) perms.departments.push({department: departments[i]._id, permissions: departments[i].permissions});
          }
          var data = {_id: $scope.community._id, name: name, locations: perms.locations, departments: perms.departments, visible: visible, moderation: moderation};
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br />Saving...'
          });
          $http.put($rootScope.URL + 'communities', data)
            .then(function(result) {
              $scope.community = data;
              $scope.community.posts = posts;
              $scope.closeEditCommunitytModal();
              $ionicLoading.hide();
            }, function(error) {
              $ionicLoading.hide();
              if(error.status != 401) {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: error.data,
                      okType: 'button-dark'
                  });
              }
            });

        }

        $scope.submitPost = function (post, files){
          console.log(post)
          console.log(files)
        }

        $ionicModal.fromTemplateUrl('modules/communities/addPost.html', {
          scope: $scope,
          animation: 'slide-in-up',
          backdropClickToClose: false
        }).then(function(modal) {
          $scope.addPostModal = modal;
        });

        $scope.openAddPostModal = function() {
          $scope.loadUsers().then(function(res) {
            $scope.users = res;
          });
          $scope.post = {
            author: $rootScope.currentUser._id,
            title: '',
            content: '',
            status: '1',
            date: new Date(),
            community: $stateParams.id,
            attachments: []
          };
          $scope.mediaArray = [];
          $scope.addPostModal.show();
        };
        $scope.closeAddPostModal = function() {
          $scope.addPostModal.hide();
        };
        $scope.files = null;

        $scope.addFiles = function (files) {
          for (var i = files.length - 1; i >= 0; i--) {
            files[i].typeOverview = files[i].type.split('/')[0];
            $scope.mediaArray.push(files[i]);
          }
          console.log($scope.mediaArray)
        }

        $scope.uploadFiles = function (post, files, edit) {
          if (!post.author) {
            $ionicPopup.alert({
                title: 'Error',
                template: 'Information missing.',
                okType: 'button-dark'
            });
            return
          }
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br />0%'
          });

          post.attachments = [];
          var oldFiles = files;
          var newFiles = files;
          for (var i = 0; i < oldFiles.length; i++) {
            if(oldFiles[i].thumbname){
              post.attachments.push(oldFiles[i]);
              files.splice(i,1);
              i--;
            }
          }

          console.log("Previously uploaded ",post.attachments);
          console.log("To be uploaded ",files);
          var count = 0;
          var total = files.length-1;

          if(files.length > 0){

            uploadFile(files[count]);
            function uploadFile (file){

              Upload.upload({
                  url: $rootScope.URL + 'upload',
                  data: {file: file}
              }).then(function (result) {

                  var data = result.data[0];
                  console.log(data)
                  post.attachments.push(data);

                  if(count >= total){
                    if(edit == true){
                      $scope.editPost(post);
                    } else {
                      $scope.addPost(post);
                    }
                  } else {
                    count++;
                    uploadFile(files[count]);
                  }

              }, function (resp) {
                  console.log(resp);
                  $ionicLoading.hide();
                  $ionicPopup.alert({
                      title: 'Error',
                      template: resp.data,
                      okType: 'button-dark'
                  });
              }, function (evt) {

                  var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  $ionicLoading.show({
                      template: '<i class="icon ion-upload"></i><br />' + progressPercentage + '%'
                  });

              });

            }



          } else {
            if(edit == true){
              $scope.editPost(post);
            } else {
              $scope.addPost(post);
            }
          }

        };

      $scope.removeAttachment = function(id) {
        $scope.mediaArray.splice(id, 1);
      }

        $scope.addPost = function(post, files) {
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br />Saving...'
          });
          $http.post($rootScope.URL + 'posts', post)
            .then(function(result) {
              $scope.closeAddPostModal();
              $scope.loadPosts(true);
              $ionicLoading.hide();
            }, function(error) {
              $ionicLoading.hide();
              if(error.status != 401) {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: error.data,
                      okType: 'button-dark'
                  });
              }
            });
        }

        $ionicModal.fromTemplateUrl('modules/communities/editPost.html', {
          scope: $scope,
          animation: 'slide-in-up',
          backdropClickToClose: false
        }).then(function(modal) {
          $scope.editPostModal = modal;
        });

        $scope.openEditPostModal = function(id) {
          $scope.loadUsers().then(function(res) {
            $scope.users = res;
          });
          $scope.post = JSON.parse(JSON.stringify($scope.community.posts[id]));
          $scope.modalAvatar = $scope.post.author.avatar;
          $scope.post.author = $scope.post.author._id;
          $scope.post.actualAuthor = $scope.post.actualAuthor._id;
          $scope.mediaArray = angular.copy($scope.post.attachments);
          $scope.editPostModal.show();
          getComments();
        };

        $scope.closeEditPostModal = function() {
          $scope.editPostModal.hide();
        };

        $scope.changeAvatar = function () {
          for (var i = 0; i < $scope.users.length; i++) {
            if($scope.users[i]._id == $scope.post.author){
                $scope.modalAvatar = $scope.users[i].avatar;
            }
          }
        }

        $scope.editPost = function(post) {
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br />Saving...'
          });
          $http.put($rootScope.URL + 'posts', post)
            .then(function(result) {
              $scope.closeEditPostModal();
              $scope.loadPosts(true);
              $ionicLoading.hide();
            }, function(error) {
              $ionicLoading.hide();
              if(error.status != 401) {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: error.data,
                      okType: 'button-dark'
                  });
              }
            });
        }

        $scope.deletePost = function(id) {
          $ionicPopup.confirm({
            title: 'Delete post',
            template: 'Are you sure you wish to delete this post?'
          }).then(function(res) {
            if(res) {
                $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br />Deleting...'
            });
            $http.delete($rootScope.URL + 'posts/' + id)
              .then(function(result) {
                $scope.loadPosts(true);
                $ionicLoading.hide();
                $scope.closeEditPostModal();
              }, function(error) {
                $ionicLoading.hide();
                if(error.status != 401) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: error.data,
                        okType: 'button-dark'
                    });
                }
              });
            }
          });
        }



        $scope.comment = {body:""};
        function getComments() {
          $scope.loadingComments = true;
          CommunitiesService.getComments($scope.post._id)
            .then(function(success){
              $scope.post.comments = success.data;
              $scope.loadingComments = false;
              console.log(success)
            }, function(error){
              console.log(error);
            })
        }

        $scope.addComment = function (comment) {
          if(comment.body.length > 1) {
            CommunitiesService.addComment($scope.post._id, comment.body)
              .then(function(success){
                $scope.comment = {body:""}
                getComments();
              }, function(error){
                console.log(error);
              })
          }

        }

        $scope.deleteComment = function (comment) {
            console.log(comment)
            // A confirm dialog
            var showConfirm = $scope.showConfirm = function() {
              $ionicPopup.confirm({
                title: 'Delete Comment',
                template: 'Are you sure you want to delete this comment?'
              }).then(function(res) {
                if(res) deleteCommentConfirmed();
              });
            };
            showConfirm();
            function deleteCommentConfirmed () {
              CommunitiesService.deleteComment(comment._id)
                .then(function(success){
                  getComments();
                }, function(error){
                  console.log(error);
                })
            }

        }



      $ionicModal.fromTemplateUrl('modules/communities/likes.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.likesModal = modal;
        });

        $scope.showLikes = function() {
          $scope.likes = [];
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
          });
          $http.get($rootScope.URL + 'likes/' + $scope.community._id)
            .then(function(result) {
              $scope.likes = result.data;
              $scope.likesModal.show();
              $ionicLoading.hide();
            }, function(error) {
              $ionicLoading.hide();
              if(error.status != 401) {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: error.data,
                      okType: 'button-dark'
                  });
              }
            });
        };
        $scope.hideLikes = function() {
          $scope.likesModal.hide();
        };

        $ionicModal.fromTemplateUrl('modules/communities/comments.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.commentsModal = modal;
        });

        $scope.showComments = function() {
          $scope.comments = [];
          $ionicLoading.show({
              template: '<ion-spinner icon="bubbles"></ion-spinner><br />Loading...'
          });
          $http.get($rootScope.URL + 'comments/' + $scope.community._id)
            .then(function(result) {
              $scope.comments = result.data;
              $scope.commentsModal.show();
              $ionicLoading.hide();
            }, function(error) {
              $ionicLoading.hide();
              if(error.status != 401) {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: error.data,
                      okType: 'button-dark'
                  });
              }
            });
        };
        $scope.hideComments = function() {
          $scope.commentsModal.hide();
        };

})
