angular.module('starter.services')
	
.service('CommunitiesService', function( $http, $q, $rootScope) {

	var host = $rootScope.URL;

	// Return public API.
	return({
	    getFeeds: getFeeds,
	    updateFeeds: updateFeeds,
	    getComments:getComments,
	    addComment:addComment,
	    deleteComment:deleteComment
	});

	function getFeeds() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"rss",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateFeeds(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"rss",
	        params: {},
	        data: {value:item}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getComments(id) {
	    var request = $http({
	        method: "get",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"comments/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addComment(id, body) {
	    var request = $http({
	        method: "post",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"comments",
	        params: {},
	        data: {post:id, body:body, date:new Date()}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function deleteComment(id) {
	    var request = $http({
	        method: "delete",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"comments/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});