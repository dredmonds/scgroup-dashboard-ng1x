angular.module('starter.services')
	
.service('RSSFeedService', function( $http, $q, $rootScope) {

	var host = $rootScope.URL;

	// Return public API.
	return({
	    getFeeds: getFeeds,
	    updateFeeds: updateFeeds,
	});

	function getFeeds() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"rss",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateFeeds(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"rss",
	        params: {},
	        data: {value:item}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});