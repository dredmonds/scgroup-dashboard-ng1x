

angular.module('starter.controllers')

.controller('RSSFeedController', function(RSSFeedService, $scope, $state, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {

	var vm = this;

	vm.feedItems = [];

	vm.showNewFeedModal = newFeedModal;
	vm.showEditFeedModal = editFeedModal;
	vm.closeFeedModal = closeFeedModal;

	vm.addFeed = addFeed;
	vm.updateFeed = updateFeed;
	vm.removeFeed = confirmRemoveFeed;

	vm.textLimit = 120;

	getFeeds();

    function getFeeds () {

	    RSSFeedService.getFeeds()
	        .then(function(success){
	            if(!success.data.value){
	            	vm.feedItems = [{"name":"","url":""}];
	            } else {
	            	vm.feedItems = success.data.value;
	            }
	            console.log(vm.feedItems)
	        }, function(error){
	          console.log(error);
	        })

	}

	function newFeedModal () {

        $ionicModal.fromTemplateUrl('modules/rssfeed/NewFeedModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up'
        }).then(function(modal) {
        	vm.feedForm = null;
            vm.newFeedModal = modal;
            vm.newFeedModal.show();
        });

	}

		function editFeedModal (item, index) {

		    $ionicModal.fromTemplateUrl('modules/rssfeed/EditFeedModal.html', {
		      	scope: $scope,
		      	animation: 'slide-in-up',
		      	backdropClickToClose: false
		    }).then(function(modal) {
		    		vm.feedForm = angular.copy(item);
		    		vm.feedIndex = index;
		        vm.newFeedModal = modal;
		        vm.newFeedModal.show();
		    });

		}

		function closeFeedModal () {
			if(vm.newFeedModal) vm.newFeedModal.remove();
		}

    function addFeed (item) {
    	if(item && item.name && item.rawUrl){

				if(item.type == 'standard'){
					item.url = item.rawUrl;
				} else if(item.type == 'custom') {
					item.url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20rss%20where%20url%3D%22'+encodeURIComponent(item.rawUrl)+'%22&format=json';
				}
	    	vm.feedItems.push(item);
	    	saveFeeds();
    	}
		}

    function updateFeed (item, index) {
			if(item.type == 'standard'){
				item.url = item.rawUrl;
			} else if(item.type == 'custom') {
				item.url = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20rss%20where%20url%3D%22'+encodeURIComponent(item.rawUrl)+'%22&format=json';
			}
    	vm.feedItems[index] = item;
    	saveFeeds();
		}

    function confirmRemoveFeed (index) {
		$ionicPopup.confirm({
			title: 'Remove RSS Feed',
			template: 'Are you sure you want to remove this RSS Feed?'
		}).then(function(res) {
			if(res) removeFeed(index);
		});

	}

    function removeFeed (index) {
    	vm.feedItems.splice(index,1);
    	saveFeeds();
	}

	function saveFeeds () {
	    RSSFeedService.updateFeeds(vm.feedItems)
	        .then(function(success){
	            getFeeds();
	            closeFeedModal();
	        }, function(error){
	        	alert(error)
	          console.log(error);
	        })
	}



});
