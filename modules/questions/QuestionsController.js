

angular.module('starter.controllers')

.controller('QuestionsController', function(QuestionsService, $scope, $state, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {
      
	var vm = this;

	// vm.feature = $scope.config.features[$stateParams.featureIndex];
	vm.questionItems = [];
	vm.questionForm = {};

	vm.currentDate = new Date().toJSON();

	vm.showQuestionModal = addQuestionModal;
	vm.showEditQuestionModal = editQuestionModal;
	vm.closeQuestionModal = closeQuestionModal;
	vm.addQuestion = addQuestion;
	vm.updateQuestion = updateQuestion;
	vm.confirmRemoveQuestion = confirmRemoveQuestion;
	
	getQuestions();

    function getQuestions () {

	    QuestionsService.getQuestions()
	        .then(function(success){
	            vm.questionItems = success.data;
	        }, function(error){
	          console.log(error);
	        })

	}

	function addQuestionModal () {

        $ionicModal.fromTemplateUrl('modules/questions/QuestionsNewItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up',
          	backdropClickToClose: false
        }).then(function(modal) {
        	vm.questionForm = {};
            vm.addQuestionModal = modal;
            vm.addQuestionModal.show();
        }); 

	}

	function editQuestionModal (item) {

        $ionicModal.fromTemplateUrl('modules/questions/QuestionsEditItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up',
          	backdropClickToClose: false	
        }).then(function(modal) {
        	vm.questionForm = angular.copy(item);
            vm.addQuestionModal = modal;
            vm.addQuestionModal.show();
        }); 

	}

	function closeQuestionModal () {
		vm.addQuestionModal.remove();
	}

	function addQuestion (item) {
		if(item && item.published == true && item.originalQuestion && item.displayQuestion && item.answer || item && item.originalQuestion) {
		    QuestionsService.addQuestion(item)
		        .then(function(success){
		            getQuestions();
		            vm.closeQuestionModal();
		        }, function(error){
		          console.log(error);
		        })
	    } else {
			$ionicPopup.alert({
				title: 'Missing fields',
				template: 'Please ensure all fields have been entered.'
			}).then(function(res) {
				// if(res) removeQuestion(item);
			});
	    }
	}

    function updateQuestion (item) {
    	if(item && item.originalQuestion && item.displayQuestion) {
		    QuestionsService.updateQuestion(item)
		        .then(function(success){
		            getQuestions();
		            vm.closeQuestionModal();
		        }, function(error){
		          console.log(error);
		        })
	    }
	}

    function confirmRemoveQuestion (item) {

		$ionicPopup.confirm({
			title: 'Delete Question',
			template: 'Are you sure you want to delete this question?'
		}).then(function(res) {
			if(res) removeQuestion(item);
		});

	}

    function removeQuestion (item) {
	    QuestionsService.removeQuestion(item)
	        .then(function(success){
	            getQuestions();
	        }, function(error){
	          console.log(error);
	        })
	}


});

