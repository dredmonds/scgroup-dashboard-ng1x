angular.module('starter.services')
	
.service('QuestionsService', function( $http, $q, $rootScope) {

	var host = $rootScope.URL;

	// Return public API.
	return({
	    getQuestions: getQuestions,
	    addQuestion: addQuestion,
	    updateQuestion: updateQuestion,
	    removeQuestion: removeQuestion,
	});


	function getQuestions() {

	    var request = $http({
	        method: "GET",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"questions",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addQuestion(item) {

	    var request = $http({
	        method: "POST",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"questions",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateQuestion(item) {

	    var request = $http({
	        method: "PUT",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"questions",
	        params: {},
	        data: item
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function removeQuestion(item) {

	    var request = $http({
	        method: "DELETE",
	        headers:{
	        	"code":$rootScope.server.pin,
	        	"token":localStorage["token"]
	        },
	        url: host+"questions/"+item._id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});