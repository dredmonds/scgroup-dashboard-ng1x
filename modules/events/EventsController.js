

angular.module('starter.controllers')

.controller('EventsController', function(EventsService, $scope, $state, $http, $ionicPopup, $ionicModal, Upload, $ionicLoading, $rootScope) {

	var vm = this;

	// vm.feature = $scope.config.features[$stateParams.featureIndex];
	vm.eventItems = [];
	vm.eventForm = {};

	vm.currentDate = new Date().toJSON();

	vm.upcomingEventsLimit = 5;
	vm.pastEventsLimit = 5;

	vm.showEventModal = addEventModal;
	vm.showEditEventModal = editEventModal;
	vm.closeEventModal = closeEventModal;
	vm.addEvent = addEvent;
	vm.updateEvent = updateEvent;
	vm.confirmRemoveEvent = confirmRemoveEvent;

  var editMode;

	getEvents();

    function getEvents () {

	    EventsService.getEvents()
	        .then(function(success){
	            vm.eventItems = success.data;
	        }, function(error){
	          console.log(error);
	        })

	}

	function addEventModal () {

        $ionicModal.fromTemplateUrl('modules/events/EventsNewItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up',
          	backdropClickToClose: false
        }).then(function(modal) {
        	vm.eventForm = {};
            vm.addEventModal = modal;
            vm.addEventModal.show();
        });

	}

	function editEventModal (item) {

        $ionicModal.fromTemplateUrl('modules/events/EventsEditItemModal.html', {
          	scope: $scope,
          	animation: 'slide-in-up',
          	backdropClickToClose: false
        }).then(function(modal) {
        	vm.eventForm = angular.copy(item);
            vm.addEventModal = modal;
            vm.addEventModal.show();
        });

	}

	function closeEventModal () {
		vm.addEventModal.remove();
	}

	  function addEvent (item, file) {

      if(item && item.title && item.additionalNotes && item.startTime && item.endTime && item.imageFile) {
        uploadFile(item)
      } else if(item && item.title && item.additionalNotes && item.startTime && item.endTime) {
		       EventsService.addEvent(item)
  		        .then(function(success){
  		            getEvents();
  		            vm.closeEventModal();
  		        }, function(error){
  		          console.log(error);
  		        })
	     } else {
    			$ionicPopup.alert({
    				title: 'Missing fields',
    				template: 'Please ensure all fields have been entered.'
    			}).then(function(res) {
    				// if(res) removeEvent(item);
    			});
	    }
	  }

    function updateEvent (item) {

      if(item && item.title && item.additionalNotes && item.startTime && item.endTime && item.imageFile) {
        editMode = true;
        uploadFile(item)
      } else if(item && item.title && item.additionalNotes && item.startTime && item.endTime) {
		       EventsService.updateEvent(item)
  		        .then(function(success){
  		            getEvents();
  		            vm.closeEventModal();
  		        }, function(error){
  		          console.log(error);
  		        })
	     } else {
    			$ionicPopup.alert({
    				title: 'Missing fields',
    				template: 'Please ensure all fields have been entered.'
    			}).then(function(res) {
    				// if(res) removeEvent(item);
    			});
	    }

	  }

    function uploadFile (item){

      Upload.upload({
          url: $rootScope.URL + 'upload',
          data: {file: item.imageFile}
      }).then(function (result) {

          $ionicLoading.hide();
          var data = result.data[0];
          console.log(data)
          item.imageFile = null;
          item.image = data;

          if(editMode == true){
              updateEvent(item);
              editMode = false;
          } else {
              addEvent(item);
          }

      }, function (resp) {
          console.log(resp);
          $ionicLoading.hide();
          $ionicPopup.alert({
              title: 'Error',
              template: resp.data,
              okType: 'button-dark'
          });
      }, function (evt) {

          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $ionicLoading.show({
              template: '<i class="icon ion-upload"></i><br />' + progressPercentage + '%'
          });

      });

    }

    function confirmRemoveEvent (item) {

		$ionicPopup.confirm({
			title: 'Delete Event',
			template: 'Are you sure you want to delete this event?'
		}).then(function(res) {
			if(res) removeEvent(item);
		});

	}

    function removeEvent (item) {
	    EventsService.removeEvent(item)
	        .then(function(success){
	            getEvents();
	            vm.closeEventModal();
	        }, function(error){
	          console.log(error);
	        })
	}


});
