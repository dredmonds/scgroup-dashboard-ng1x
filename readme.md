**StaffConnect Dashboard**

Setup

 - In a Terminal in the root directory of the project
 - Install http-server
 -  - 	sudo npm install -g http-server
 - Install Node Modules
 -  - 	npm install

Preview
In a Terminal in the root directory of the project

 - http-server -c-1

This will start a web server with caching disabled.
